
import unittest
import datetime

from utilities.signal.df_signal import DFSignal


class TestDFSignal(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_validateKeyInKwargs_KeyNotInKwargs_returnsNone(self):
        """Test if the key passed isn't in kwargs then None is returned"""
        # arrange
        kwargs = {"string": "string", "int": 1, "float": 1.5, "date": datetime.datetime.now()}
        # act
        result = DFSignal.validateKeyInkwargs("key", str, **kwargs)
        # assert
        self.assertIsNone(result)


    def test_validateKeyInKwargs_KeyIsKwargs_validtType_RightObjectReturned(self):
        """Test valid use case. Test that if the  key passed is in kwargs and the result is of the right type
        then this object is returned"""
        kwargs = {"string": "string", "int": 1, "float": 1.5, "date": datetime.datetime.now()}

        testCases = [
            ("string", str),
            ("int", int),
            ('float', float),
            ('date', datetime.datetime) #datetime included to test arbitrary type
        ]
        for key, type in testCases:
            with self.subTest(name=key):
                result = DFSignal.validateKeyInkwargs(key, type, **kwargs)
                # assert
                self.assertIsInstance(result, type)

    def test_validateKeyInKwargs_KeyIsKwargs_invalidType_TypeErrorThrown(self):
        """Test invalid use case. Test that if the key passed is in kwargs  but the result is of the wrong type
                then a TypeError is Thrown"""
        kwargs = {"string": 1, "int": "string", "float": datetime.datetime.now(), "date": 1.5}

        testCases = [
            ("string", str),
            ("int", int),
            ('float', float),
            ('date', datetime.datetime)  # datetime included to test arbitrary type
        ]
        for key, type in testCases:
            with self.subTest(name=key):
                with self.assertRaises(TypeError):
                    DFSignal.validateKeyInkwargs(key, type, **kwargs)

if __name__ == '__main__':
    unittest.main()
