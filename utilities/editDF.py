import pandas as pd
# self made functions
import utils as u


class EditDF():

	def readMultipleCSV(csvNames):
		""" Function combines different csv and returns DF """
		r = pd.read_csv(csvNames[0] + '.csv', index_col='Unnamed: 0')
		for c in range(1, len(csvNames)):
			new = pd.read_csv(csvNames[c] + '.csv', index_col='Unnamed: 0')
			r = pd.concat([r, new], axis=1, join='inner')
		return r

	def drop(resistance, dropDates):
		""" Function drops date column from the dataframe """
		for d in dropDates:
			resistance = resistance.drop(d, axis=1)
		return resistance

	def parseIndex(resistance):
		""" Function parses index for sector """
		a = sorted(resistance.index.values, key=lambda x: (x[-9], x[-7], x[-13:-10]))
		resistance = resistance.reindex(index=a)
		return resistance

	def time(bigSignals, typ, offset=0):
		""" Function builds chosen type of time vektor """
		if (typ == 'date') or (typ == 'offset'):
			date = bigSignals.columns.values
			date = [pd.Timestamp(date[i]) for i in range(len(date))]
			if typ == 'offset':
				t = [str(pd.Timestamp(d) + pd.DateOffset(minutes=offset)) for d in date]
				return t
			else:
				return date
		elif typ == 'years':
			x = bigSignals.columns.values
			x = [u.datetime2unixtimeInSecond(pd.Timestamp(x[i])) for i in range(len(x))]
			x[:] = [t - x[0] for t in x]
			x[:] = [t / (3600 * 24 * 365) for t in x]
			return x

		elif typ == 'days':
			x = bigSignals.columns.values
			x = [u.datetime2unixtimeInSecond(pd.Timestamp(x[i])) for i in range(len(x))]
			x[:] = [t - x[0] for t in x]
			x[:] = [t / (3600 * 24) for t in x]
			return x
		else:
			print(typ + ' is not a supported type')

	def timeFromBmode(bMode, Time):  # in order to get other timestamps than 6.0
		""" Function returns important ramp times"""
		triggerLow = pd.Timestamp(bMode.iloc[1][Time])
		triggerHigh = pd.Timestamp(bMode.iloc[2][Time])
		start = triggerLow + pd.DateOffset(minutes=-30)
		end = triggerHigh + pd.DateOffset(minutes=30)
		return (start, triggerLow, triggerHigh, end)

	def refresh(bigSignals, filteredSignal):
		""" Function refreshes Dataframe if format is right"""
		if str(type(filteredSignal.result)) == "<class 'pandas.core.frame.DataFrame'>":
			print('Signals refreshed')
			return filteredSignal.result
		else:
			print('Signals not refreshed')
			return bigSignals

	def smallValueDates(snr, margin):
		""" Function returns all dates where the value is smaller than the margin """
		cut = []
		for i in range(len(snr.values)):
			if float(snr.values[i]) <= margin:
				cut.append(snr.index[i])
		return cut
