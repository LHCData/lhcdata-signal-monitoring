
import unittest
import influxdb
from unittest.mock import patch
from utilities.influx.signalInfluxDb import Signal as SignalInflux
from influxdb import DataFrameClient
import pandas as pd
from utilities.signal.signal import Signal
from copy import deepcopy
from utilities.signal.dataFrameIndexConverter import  DataFrameIndexConverter

class TestSignalInflux(unittest.TestCase):

    def setUp(self):
        self.defaultClient = DataFrameClient('dbod-lhc-sm.cern.ch', 8081, 'root', 'root', 'test', ssl=True)
        self.uut = SignalInflux(deepcopy(self.defaultClient))
        self.newClient = DataFrameClient('localhost', 1000, 'user', 'password', 'mydb', ssl=True)
        self.df = self.createTestDataFrame()
        self.signal = Signal("test", self.df)
        self.influxDF = deepcopy(self.df)
        DataFrameIndexConverter.convertIndexFromNanosecondsToTimestamps(self.influxDF)

    def tearDown(self):
        pass

    def createTestDataFrame(self):
        """Method creates a simple DataFrame object"""
        data = [1.0, 1.2, 1.4]

        df = pd.DataFrame(data, [0,1,2], columns=['value'])
        return df


    def assertDefaultClient(self, otherClient):
        self.assertEqual(otherClient._username, self.defaultClient._username)
        self.assertEqual(otherClient._password, self.defaultClient._password)
        self.assertEqual(otherClient._host, self.defaultClient._host)
        self.assertEqual(otherClient._port, self.defaultClient._port)
        self.assertEqual(otherClient._database, self.defaultClient._database)

    def assertNewClient(self, otherClient):
        self.assertEqual(otherClient._username, self.newClient._username)
        self.assertEqual(otherClient._password, self.newClient._password)
        self.assertEqual(otherClient._host, self.newClient._host)
        self.assertEqual(otherClient._port, self.newClient._port)
        self.assertEqual(otherClient._database, self.newClient._database)

    def test_init_rightDefaultClientIsCreated(self):
        """Tests that the right default client is created when a Signal is initialized"""
        self.assertDefaultClient(self.uut.client)

    # region createDataFrameClient tests

    def test_createDataFrameClient_NoParams_ReturnsDefaultClient(self):
        """Tests that when createDataFrameClient method is without passed parameters, a default client is returned.
           The default client is defined as:
                username : ""
                password : ""
                host     : dbod-lhc-sm.cern.ch
                port     : 8081
                database : test
        """

        # act:
        client = SignalInflux.createDataFrameClient()

        # assert:
        self.assertEqual(client._username, '')
        self.assertEqual(client._password, '')
        self.assertEqual(client._host, self.defaultClient._host)
        self.assertEqual(client._port, self.defaultClient._port)
        self.assertEqual(client._database, self.defaultClient._database)

    def test_createDataFrameClient_ArbitraryParams_ReturnCorrectClient(self):
        """Tests that when createDataFrameClient method  is called with arbitrary prameters, the correct client is returned"""

        # arrange
        testCases = [
            ('user', 'password', 'localhost', 8086, 'mydb'),
        ]

        # uses subTest to test a set of test cases:
        for user, password, host, port, database in testCases:
            with self.subTest(name=user + "-" + password + "-" + host + "-" + str(port) + "-" + database):
                # act:
                client = SignalInflux.createDataFrameClient(user, password, host, port, database)

                # assert:
                self.assertEqual(client._username, user)
                self.assertEqual(client._password, password)
                self.assertEqual(client._host, host)
                self.assertEqual(client._port, port)
                self.assertEqual(client._database, database)

    # endregion

    #region createSignalInflux tests

    def test_createSignal_clientKeyProvided_InfluxSignalCreatedWithCorrectClient(self):
        """Test that when kwargs contain valid arguments a correct client is created and overwrites the current
        client"""

        influxSignal = SignalInflux.createSignalInflux(client=deepcopy(self.newClient))

        # assert:
        self.assertNewClient(influxSignal.client)

    def test_createSignal_clientArgumentKeysProvided_InfluxSignalCreatedWithCorrectClient(self):
        """Test that when kwargs contain valid arguments a correct client is created and overwrites the current
        client"""

        influxSignal = SignalInflux.createSignalInflux(host='localhost',
                                                       port=1000,
                                                       username='user',
                                                       password='password',
                                                       database='mydb')

        # assert:
        self.assertNewClient(influxSignal.client)

    def test_createSignal_clientArgumentKeysNotProvided_InfluxSignalCreatedWithDefaultClient(self):
        """Test that when kwargs contain valid arguments a correct client is created and overwrites the current
        client"""

        influxBehavior = SignalInflux.createSignalInflux()

        # assert:
        self.assertDefaultClient(influxBehavior.client)

    def test_createSignal_clientKeyAndClientArgumentKeysProvided_InfluxSignalCreatedWithCorrectClient(self):
        """Test that when kwargs contain valid arguments a correct client is created and overwrites the current
        client"""

        influxBehavior = SignalInflux.createSignalInflux(host='host',
                                                         port=2000,
                                                         username='username',
                                                         assword='password2',
                                                         database='otherdb',
                                                         client=deepcopy(self.newClient))

        # assert:
        self.assertNewClient(influxBehavior.client)

    #region TypeError thrown if arguments are invalid

    def test_createSignal_UsernameIsInvalidType_TypeErrorIsThrown(self):
        """Tests that when kwargs contain a username keyword that isn't a string, a TypeError
        is thrown"""

        with self.assertRaises(TypeError):
            SignalInflux.createSignalInflux(username=1000, password="password", host="host", port=1000,
                                            database="mydb")

    def test_createSignal_PasswordIsInvalidType_TypeErrorIsThrown(self):
        """Tests that when kwargs contain a password keyword that isn't a string, a TypeError
        is thrown"""

        with self.assertRaises(TypeError):
            SignalInflux.createSignalInflux(username='user', password=1000, host="host", port=1000, database="mydb")

    def test_createSignal_HostIsInvalidType_TypeErrorIsThrown(self):
        """Tests that when kwargs contain a host keyword that isn't a string, a TypeError
        is thrown"""

        with self.assertRaises(TypeError):
            SignalInflux.createSignalInflux(username="user", password="password", host=1000, port=1000,
                                            database="mydb")

    def test_createSignal_PortIsInvalidType_TypeErrorIsThrown(self):
        """Tests that when kwargs contain a port keyword that isn't an integer, a TypeError
        is thrown"""

        with self.assertRaises(TypeError):
            SignalInflux.createSignalInflux(username="user", password="password", host="host", port="port",
                                            database="mydb")

    def test_createSignal_DatabaseIsInvalidType_TypeErrorIsThrown(self):
        """Tests that when kwargs contain a database keyword that isn't a string, a TypeError
        is thrown"""

        with self.assertRaises(TypeError):
            SignalInflux.createSignalInflux(username="user", password="password", host="host", port=1000,
                                            database=1000)

    # endregion

    #endregion

    #region write tests

    def test_write_NoArguments_writesCorrectToDefaultDatabase(self):
        """Test that write method  writes correctly to the database using the default client if no arguments are passed"""
        # arrange done in setUp

        with patch.object(influxdb.DataFrameClient, 'write_points') as mock_write_points:
            mock_write_points.return_value = True

            # act
            result = self.uut.write(self.signal)

            # assert client's write_points method is called right arguments
            mock_write_points.assert_called_with(dataframe=self.signal.df,
                                                 measurement='test',
                                                 tags={},
                                                 protocol='json',
                                                 time_precision='n')

            # assert that right write method correctly returns True on success
            self.assertTrue(result)

    def test_write_tagSetProvided_writesCorrectlyToDefaultDatabase(self):
        """Test that write method writes correctly to the database using the default client if a tag set is given"""
        # test signal is prepared in setUp

        with patch.object(influxdb.DataFrameClient, 'write_points') as mock_write_points:
            mock_write_points.return_value = True

            # act
            result = self.uut.write(self.signal, tags={'tagKey': 'tagValue'})

            # assert client's write_points method is called right arguments
            mock_write_points.assert_called_with(dataframe=self.signal.df,
                                                 measurement='test',
                                                 tags={'tagKey': 'tagValue'},
                                                 protocol='json',
                                                 time_precision='n')

            # assert that right write method correctly returns True on success
            self.assertTrue(result)

    #endregion

    #region read tests

    def test_read_OnlySignalArgumentProvided_readsCorrectlyFromDataBase(self):
        """Test that read methpod reads correctly from the database if no arguments are passed"""
        # test signal is prepared in setUp

        with patch.object(influxdb.DataFrameClient, 'query') as mock_query:
            mock_query.return_value = {'test': deepcopy(self.influxDF)}

            # act
            result = self.uut.read(self.signal)

            # assert client's read method is called right arguments
            mock_query.assert_called_with('select * from test')

            # assert that valid method correctly returns a correctly formatted DataFrame
            pd.testing.assert_frame_equal(self.df, result)

    def test_read_signalNameParameterProvided_readsCorrectlyFromDataBase(self):
        """Test that read method reads correctly from the database signalName argument is passed"""
        # test signal is prepared in setUp

        with patch.object(influxdb.DataFrameClient, 'query') as mock_query:
            mock_query.return_value = {'signalName': deepcopy(self.influxDF)}

            # act
            result = self.uut.read(self.signal, signalName='signalName')

            # assert client's read method is called right arguments
            mock_query.assert_called_with('select * from signalName')

            # assert that valid method correctly returns a correctly formatted DataFrame
            pd.testing.assert_frame_equal(self.df, result)

    def test_read_signalNameAndQueryParameterProvided_readsCorrectlyFromDataBase(self):
        """Test that read method reads correctly from the database if both signalName and Query are passed"""
        # test signal is prepared in setUp

        with patch.object(influxdb.DataFrameClient, 'query') as mock_query:
            mock_query.return_value = {'signalName': deepcopy(self.influxDF)}

            # act
            result = self.uut.read(self.signal, signalName='signalName', query="select * from signalName where val > 2")

            # assert client's read method is called right arguments
            mock_query.assert_called_with("select * from signalName where val > 2")

            # assert that valid method correctly returns a correctly formatted DataFrame
            pd.testing.assert_frame_equal(self.df, result)

    def test_read_QueryParameterProvided_readsReturnNone(self):
        """Test that read method returns None if only query argument is passed"""
        # test signal is prepared in setUp

        with patch.object(influxdb.DataFrameClient, 'query') as mock_query:
            mock_query.return_value = {'signalName': deepcopy(self.influxDF)}

            # act
            result = self.uut.read(self.signal, query="select * from signalName where val > 2")

            # assert that valid method correctly returns a correctly formatted DataFrame
            self.assertIsNone(result)

    #endregion

if __name__ == '__main__':
    unittest.main()
