# """Tutorial for using pandas and the InfluxDB client. From https://github.com/influxdata/influxdb-python/blob/master/examples/tutorial_pandas.py"""
# import pandas as pd
# from influxdb import DataFrameClient, InfluxDBClient
# import time
#
# # client = InfluxDBClient(host='dbod-lhc-sm.cern.ch',
# #                         port=8081,
# #                         username='root',
# #                         password='root',
# #                         database='test',
# #                         ssl=True)
#
# dfClient = DataFrameClient(host='dbod-lhc-sm.cern.ch',
#                         port=8081,
#                         username='root',
#                         password='root',
#                         database='test',
#                         ssl=True)
#
# print("Create pandas DataFrame")
# df = pd.DataFrame(data=list(range(30)),
#                   index=pd.date_range(start='2014-11-16',
#                                       periods=30, freq='H'), columns=['0'])
#
# print(df)
#
# print("Write DataFrame")
# dfClient.write_points(df, 'demo', protocol='json')
# #
# # #print("Write DataFrame with Tags")
# # #client.write_points(df, 'demo', {'k1': 'v1', 'k2': 'v2'}, protocol='json')
# #
# # time.sleep(3)
# #
# # print("Read DataFrame")
# # q = "select * from demo"
# # df = pd.DataFrame(client.query(q, chunked=True, chunk_size=10000).get_points())
# # print(df)
# # print('hey')
#
#

import argparse
import pandas as pd

from influxdb import DataFrameClient

"""Instantiate the connection to the InfluxDB client."""
user = 'root'
password = 'root'
dbname = 'test'
protocol = 'json'

client = DataFrameClient('dbod-lhc-sm.cern.ch', 8081, user, password, dbname)

print("Create pandas DataFrame")
df = pd.DataFrame(data=list(range(30)),
                  index=pd.date_range(start='2014-11-16',
                                      periods=30, freq='H'), columns=['0'])

print("Create database: " + dbname)
client.create_database(dbname)

print("Write DataFrame")
client.write_points(df, 'demo', protocol=protocol)


print("Read DataFrame")
client.query("select * from demo")





