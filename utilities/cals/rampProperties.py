import numpy as np
import pandas as pd
import signalAnalysis as sa
import signalCALS as scals
import utils as u


# This class is an adoption of the class "DetectBmodePattern". It calculates more parameters than only the bmode times and is therefore slower.
# Other than DetectBmodePattern, RampProperties also returns a DF in the norm format (index=signals, columns= timestamp @bmode_6.0)
class RampProperties(object):

	def __init__(self, startTime={}, endTime={}, bMode={}, voltage={}, current={}, trigger={}):
		self.StartTime = startTime
		self.EndTime = endTime
		self.BMode = bMode
		self.Voltage = voltage
		self.Current = current
		self.Trigger = trigger

	def getProperties(self):
		""" Function looks for ramp properties: Timestamps and SNR """
		# look for the whole DataFrame of the chosen bMode
		bModeDF = scals.Signal(self.BMode, self.StartTime, self.EndTime).getSignal(self.BMode)
		if self.Trigger == {}: self.Trigger = [2.0, 6.0, 8.0, 14.0]
		bModeDF = bModeDF[bModeDF[self.BMode].isin(self.Trigger)]
		triggerDF = self.Filter(bModeDF)

		# DF is ready, now the four ramp timestamps can be defined
		stamp = np.zeros((4, len(triggerDF.iloc[0].values)))
		for i in range(len(self.Trigger)):
			stamp[i] = triggerDF.iloc[i].values
		postRampDown, triggerLowStamp, triggerHighStamp, preRampDown = self.toDateTime(stamp)

		# calculate snr of the voltage&current before beam injection and after stable beam
		voltageSNRlow, voltageSNRhigh, currentSNRlow, currentSNRhigh = self.snr(triggerLowStamp, triggerHighStamp)

		# define DF which is in the general format
		returnDF = pd.DataFrame(
			data=[postRampDown, triggerLowStamp, triggerHighStamp, preRampDown, voltageSNRlow, voltageSNRhigh,
			      currentSNRlow, currentSNRhigh],
			index=['BMode_2.0', 'BMode_6.0', 'BMode_8.0', 'BMode_14.0', 'voltageSNRlow', 'voltageSNRhigh',
			       'currentSNRlow', 'currentSNRhigh'],
			columns=triggerLowStamp)
		return returnDF

	def Filter(self, bModeDF):
		""" Function filters out the invalid ramps """
		triggerDF = pd.DataFrame(data=np.zeros(len(self.Trigger)), index=self.Trigger)
		i = 1
		toShort = 0
		incomplete = 0
		# only take signals wich have all bmodes in the right order in it
		for b in range(4, len(bModeDF[self.BMode])):
			if all([bModeDF[self.BMode].iloc[b - 3 + t] == self.Trigger[t] for t in range(len(self.Trigger))]):
				if (bModeDF[self.BMode].index[b - 2] - bModeDF[self.BMode].index[b - 3] >= 1800) and \
						(bModeDF[self.BMode].index[b] - bModeDF[self.BMode].index[b - 1] >= 1800):
					newFrame = pd.DataFrame(bModeDF[self.BMode].index[b - 3:b + 1], index=self.Trigger)
					newFrame.columns = [i]
					i += 1
					triggerDF = pd.concat([triggerDF, newFrame], axis=1, join='inner')
				else:
					toShort += 1
			else:
				incomplete += 1

		incomplete = incomplete - (i - 2) * 4
		print(str(incomplete) + " triggers have been skiped because of incompleteness")
		print(str(toShort) + " ramps have not been taken because of to short up/down time")
		del triggerDF[0]

		return triggerDF

	def toDateTime(self, stamp):
		""" Function filters out the invalid ramps """
		postRampDown = [u.unixTimeInNanoSecond2datatime(float(stamp[0][s]) * 1e9) for s in range(len(stamp[1]))]
		triggerLowStamp = [u.unixTimeInNanoSecond2datatime(float(stamp[1][s]) * 1e9) for s in range(len(stamp[1]))]
		triggerHighStamp = [u.unixTimeInNanoSecond2datatime(float(stamp[2][s]) * 1e9) for s in range(len(stamp[1]))]
		preRampDown = [u.unixTimeInNanoSecond2datatime(float(stamp[3][s]) * 1e9) for s in range(len(stamp[1]))]

		return postRampDown, triggerLowStamp, triggerHighStamp, preRampDown

	def snr(self, triggerLowStamp, triggerHighStamp):
		""" Function calculates snr of the voltage&current before beam injection and after stable beam """
		voltageSNRlow = np.zeros(len(triggerLowStamp))
		voltageSNRhigh = np.zeros(len(triggerLowStamp))
		currentSNRlow = np.zeros(len(triggerLowStamp))
		currentSNRhigh = np.zeros(len(triggerLowStamp))

		for i in range(len(triggerLowStamp)):
			(fullVoltage, lowVoltage, highVoltage) = sa.SignalAnalysis.getSignalParts(triggerLow=triggerLowStamp[i],
			                                                                          Location=self.Voltage,
			                                                                          triggerHigh=triggerHighStamp[i])
			(fullVoltage, lowCurrent, highCurrent) = sa.SignalAnalysis.getSignalParts(triggerLow=triggerLowStamp[i],
			                                                                          Location=self.Current,
			                                                                          triggerHigh=triggerHighStamp[i])

			voltageSNRlow[i] = sa.SignalAnalysis.snr(lowVoltage.values)  # lower part of the Voltage has lower SNR
			voltageSNRhigh[i] = sa.SignalAnalysis.snr(highVoltage.values)
			currentSNRlow[i] = sa.SignalAnalysis.snr(lowCurrent.values)
			currentSNRhigh[i] = sa.SignalAnalysis.snr(highCurrent.values)

			print(str(triggerLowStamp[i]) + ' SNR calculated successfull')

		return list(voltageSNRlow), list(voltageSNRhigh), list(currentSNRlow), list(currentSNRhigh)
