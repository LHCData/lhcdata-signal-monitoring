import re

class RbMetadata:
    rbCircuitToSignalMetadata =  {
        'RB.A12': {'circuitName' :    'RPTE.UA23.RB.A12',
                   'signalFPA' :      'RB.A12.EVEN:ST_ABORT_PIC',
                   'signalFGC_ABS' :  'RPTE.UA23.RB.A12:I_MEAS',
                   'signalFGC_PCNT' : 'RPTE.UA23.RB.A12:I_EARTH_PCNT'},
        'RB.A23': {'circuitName' :    'RPTE.UA27.RB.A23',
                   'signalFPA' :      'RB.A23.EVEN:ST_ABORT_PIC',
                   'signalFGC_ABS' :  'RPTE.UA27.RB.A23:I_MEAS',
                   'signalFGC_PCNT' : 'RPTE.UA27.RB.A23:I_EARTH_PCNT'},
        'RB.A34': {'circuitName' :    'RPTE.UA43.RB.A34',
                   'signalFPA' :      'RB.A34.EVEN:ST_ABORT_PIC',
                   'signalFGC_ABS' :  'RPTE.UA43.RB.A34:I_MEAS',
                   'signalFGC_PCNT' : 'RPTE.UA43.RB.A34:I_EARTH_PCNT'},
        'RB.A45': {'circuitName' :    'RPTE.UA47.RB.A45',
                   'signalFPA' :      'RB.A45.EVEN:ST_ABORT_PIC',
                   'signalFGC_ABS' :  'RPTE.UA47.RB.A45:I_MEAS',
                   'signalFGC_PCNT' : 'RPTE.UA47.RB.A45:I_EARTH_PCNT'},
        'RB.A56': {'circuitName' :    'RPTE.UA63.RB.A56',
                   'signalFPA' :      'RB.A56.EVEN:ST_ABORT_PIC',
                   'signalFGC_ABS' :  'RPTE.UA63.RB.A56:I_MEAS',
                   'signalFGC_PCNT' : 'RPTE.UA63.RB.A56:I_EARTH_PCNT'},
        'RB.A67': {'circuitName' :    'RPTE.UA67.RB.A67',
                   'signalFPA' :      'RB.A67.EVEN:ST_ABORT_PIC',
                   'signalFGC_ABS' :  'RPTE.UA67.RB.A67:I_MEAS',
                   'signalFGC_PCNT' : 'RPTE.UA67.RB.A67:I_EARTH_PCNT'},
        'RB.A78': {'circuitName' :    'RPTE.UA83.RB.A78',
                   'signalFPA' :      'RB.A78.EVEN:ST_ABORT_PIC',
                   'signalFGC_ABS' :  'RPTE.UA83.RB.A78:I_MEAS',
                   'signalFGC_PCNT' : 'RPTE.UA83.RB.A78:I_EARTH_PCNT'},
        'RB.A81': {'circuitName' :    'RPTE.UA87.RB.A81',
                   'signalFPA' :      'RB.A81.EVEN:ST_ABORT_PIC',
                   'signalFGC_ABS' :  'RPTE.UA87.RB.A81:I_MEAS',
                   'signalFGC_PCNT' : 'RPTE.UA87.RB.A81:I_EARTH_PCNT'}}
    
    # RB.A12 => magnets xxxR1 & xxxL2
    # RB.A23 => magnets xxxR2 & xxxL3
    # RB.A34 => magnets xxxR3 & xxxL4
    # RB.A45 => magnets xxxR4 & xxxL5
    # RB.A56 => magnets xxxR5 & xxxL6
    # RB.A67 => magnets xxxR6 & xxxL7
    # RB.A78 => magnets xxxR7 & xxxL8
    # RB.A81 => magnets xxxR8 & xxxL1
    magnetNameToArc = {"R1": "RB.A12", "L2": "RB.A12",
                   "R2": "RB.A23", "L3": "RB.A23",
                   "R3": "RB.A34", "L4": "RB.A34",
                   "R4": "RB.A45", "L5": "RB.A45",
                   "R5": "RB.A56", "L6": "RB.A56",
                   "R6": "RB.A67", "L7": "RB.A67",
                   "R7": "RB.A78", "L8": "RB.A78",
                   "R8": "RB.A81", "L1": "RB.A81"}
    
    @staticmethod
    def getArcNameFromMagnetName(key):
        regExp = "[LR]{1}\d{1}"
        foundRegExp = re.findall(regExp, key)
        if len(foundRegExp) != 1:
            raise ValueError("Key {} does not contain proper MB magnet name".format(key))
        else:
            return RbMetadata.magnetNameToArc[foundRegExp[0]]
    
    @staticmethod
    def getMetadata(keys='*'):
        if len(keys) < 1:
            raise ValueError("keys can not be empty!")

        if len(keys) == 1 and keys[0] == '*':
            return RbMetadata.rbCircuitToSignalMetadata

        for key in keys:
            if key not in RbMetadata.rbCircuitToSignalMetadata.keys():
                raise ValueError("Key {} does not exist!".format(key))

        return {key: RbMetadata.rbCircuitToSignalMetadata[key] for key in keys}

class QhPmMetadata:
    system = 'QPS'
    source = '*'

    qhMagnetToPmSignalMetadata = {
        # MB with 4 QH per magnet (2 apertures)
        'MB': {'className': 'DQAMCNMB_PMHSU',
               'sourcePrefix': 'MB_',
               'magnetNameRegExp': '[ABC]{1}\d+[LR]{1}[1-8]{1}',
               'uNames': ['U_HDS_1', 'U_HDS_2', 'U_HDS_3', 'U_HDS_4'],
               'iNames': ['I_HDS_1', 'I_HDS_2', 'I_HDS_3', 'I_HDS_4'],
               'rNames': ['R_HDS_1', 'R_HDS_2', 'R_HDS_3', 'R_HDS_4'],
               'dRNames': ['dR_HDS_1', 'dR_HDS_2', 'dR_HDS_3', 'dR_HDS_4'],
               'filterNames': ['U_HDS_1']},
        # MQ with 1 QH per aperture (current always measured as zero)
        'MQ': {'className': 'DQAMCNMQ_PMHSU',
               'sourcePrefix': 'MQ_',
               'magnetNameRegExp': '\d+[LR]{1}[1-8]{1}',
               'uNames': ['U_HDS_1', 'U_HDS_2'],
               'iNames': [],
               'filterNames': []},
        # RQX triplets (Q1, Q2, Q3)
        'RQX': {'className': 'DQAMGNC',
                'sourcePrefix': '',
                'magnetNameRegExp': '[R][Q][X][.][LR]{1}[1-8]{1}',
                'uNames': ['U_HDS_1_Q1', 'U_HDS_2_Q1', 'U_HDS_1_Q2', 'U_HDS_2_Q2', 'U_HDS_3_Q2', 'U_HDS_4_Q2',
                           'U_HDS_1_Q3', 'U_HDS_2_Q3'],
                'iNames': [],
                'rNames': [],
                'dRNames': [],
                'filterNames': []},
        # IPQ and IPD with 1 QH per aperture
        'IPQD2': {'className': 'DQAMGNRQA',
                  'sourcePrefix': '',
                  'magnetNameRegExp': '[R]{1}[QD]{1}\d+[.][LR]{1}[1-8]{1}',
                  'uNames': ['U_HDS_1_B1', 'U_HDS_1_B2'],
                  'iNames': [],
                  'rNames': [],
                  'dRNames': [],
                  'filterNames': []},
        # IPQ and IPD with 2 QH per aperture
        'IPQD4': {'className': 'DQAMGNRQB',
                  'sourcePrefix': '',
                  'magnetNameRegExp': '[R]{1}[QD]{1}\d+[.][LR]{1}[1-8]{1}',
                  'uNames': ['U_HDS_1_B1', 'U_HDS_2_B1', 'U_HDS_1_B2', 'U_HDS_2_B2'],
                  'iNames': [],
                  'rNames': [],
                  'dRNames': [],
                  'filterNames': []},
        # IPQ with 4 QH per aperture (only MQY?)
        'IPQD8': {'className': 'DQAMGNRQC',
                  'sourcePrefix': '',
                  'magnetNameRegExp': '[R]{1}[QD]{1}\d+[.][LR]{1}[1-8]{1}',
                  'uNames': ['U_HDS_1_B1', 'U_HDS_2_B1', 'U_HDS_3_B1', 'U_HDS_4_B1', 'U_HDS_1_B2', 'U_HDS_2_B2',
                             'U_HDS_3_B2', 'U_HDS_4_B2'],
                  'iNames': [],
                  'rNames': [],
                  'dRNames': [],
                  'filterNames': []}
        }

    @staticmethod
    def getMetadata(keys='*'):
        if len(keys) < 1:
            raise ValueError("keys can not be empty!")

        if len(keys) == 1 and keys[0] == '*':
            return QhPmMetadata.qhMagnetToPmSignalMetadata

        for key in keys:
            if key not in QhPmMetadata.qhMagnetToPmSignalMetadata.keys():
                raise ValueError("Key {} does not exist!".format(key))

        return {key: QhPmMetadata.qhMagnetToPmSignalMetadata[key] for key in keys}
