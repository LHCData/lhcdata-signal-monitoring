import pandas as pd


def Shift2_Zero(DF):
	IndexShift = DF.index[:] - DF.index[0]
	Value = DF.values
	Shift_DF = pd.DataFrame(data=Value, index=IndexShift)
	return Shift_DF


def Pre_Processing(FirstSignal, SamplingRate):
	# Shifting to zero
	FirstShift = Shift2_Zero(FirstSignal)

	# Drop Nan
	FirstShift = FirstShift.dropna()

	#  Convert to datetime

	Dt_First = FirstShift
	Dt_First.index = pd.to_datetime(Dt_First.index, unit='s')

	# Resample

	resampledFirst = Dt_First.resample(str(SamplingRate) + 's').ffill(limit=1).interpolate()

	# Eleiminate the first element (usually NaN)
	resampledFirst = resampledFirst[1:]

	return resampledFirst
