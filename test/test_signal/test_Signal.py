import unittest
from unittest import mock
from unittest.mock import patch
from utilities.influx import signalInfluxDb
from utilities.signal import df_signal
from utilities.cals import signalCALS
from utilities.post_mortem import signalPM
from utilities.signal.signal import Signal, DataFrameIndexState
import numpy as np
import pandas as pd
from pandas.testing import assert_frame_equal
from datetime import timedelta, datetime
from copy import deepcopy


class TestSignal(unittest.TestCase):

    def setUp(self):
        self.uut = self.createTestSignal()

        self.mockLoggingDB = mock.Mock()

        self.unixTimeSeconds = [
            1534597678.6200001, 1534597679.1200001, 1534597738.6200001, 1534597739.1200001, 1534597798.6200001,
            1534597799.1200001, 1534597829.6200001, 1534597830.1200001, 1534597833.6200001, 1534597834.1200001,
            1534597836.6200001, 1534597837.1200001, 1534597839.1200001, 1534597839.6200001, 1534597841.1200001,
            1534597841.6200001, 1534597843.1200001, 1534597843.6200001, 1534597845.1200001, 1534597845.6200001,
            1534597847.1200001, 1534597847.6200001, 1534597848.6200001, 1534597849.1200001, 1534597850.1200001,
            1534597850.6200001, 1534597851.6200001, 1534597852.1200001, 1534597853.1200001, 1534597853.6200001,
            1534597854.6200001, 1534597855.1200001, 1534597856.1200001, 1534597856.6200001, 1534597857.6200001,
            1534597858.1200001, 1534597859.1200001, 1534597859.6200001, 1534597860.1200001, 1534597860.6200001,
            1534597861.1200001, 1534597861.6200001, 1534597862.1200001, 1534597862.6200001, 1534597863.1200001,
            1534597863.6200001, 1534597864.1200001, 1534597864.6200001, 1534597865.1200001, 1534597865.6200001,
            1534597866.1200001, 1534597866.6200001, 1534597867.1200001, 1534597867.6200001, 1534597868.1200001,
            1534597868.6200001, 1534597869.1200001, 1534597869.6200001, 1534597870.1200001, 1534597870.6200001,
            1534597871.1200001, 1534597871.6200001, 1534597872.1200001, 1534597872.6200001, 1534597873.1200001,
            1534597873.6200001, 1534597874.1200001, 1534597874.6200001, 1534597875.1200001, 1534597875.6200001,
            1534597876.1200001, 1534597876.6200001, 1534597877.1200001, 1534597877.6200001, 1534597878.1200001,
            1534597878.6200001, 1534597879.1200001, 1534597879.6200001, 1534597880.1200001, 1534597880.6200001,
            1534597881.1200001, 1534597881.6200001, 1534597882.1200001, 1534597882.6200001, 1534597883.1200001,
            1534597883.6200001, 1534597884.1200001, 1534597884.6200001, 1534597885.1200001, 1534597885.6200001,
            1534597886.1200001, 1534597886.6200001, 1534597887.1200001, 1534597887.6200001, 1534597888.1200001,
            1534597888.6200001, 1534597889.1200001, 1534597889.6200001, 1534597890.1200001, 1534597890.6200001]
        self.unixTimeNanoseconds = [
            1534597678620000000, 1534597679120000000, 1534597738620000000, 1534597739120000000, 1534597798620000000,
            1534597799120000000, 1534597829620000000, 1534597830120000000, 1534597833620000000, 1534597834120000000,
            1534597836620000000, 1534597837120000000, 1534597839120000000, 1534597839620000000, 1534597841120000000,
            1534597841620000000, 1534597843120000000, 1534597843620000000, 1534597845120000000, 1534597845620000000,
            1534597847120000000, 1534597847620000000, 1534597848620000000, 1534597849120000000, 1534597850120000000,
            1534597850620000000, 1534597851620000000, 1534597852120000000, 1534597853120000000, 1534597853620000000,
            1534597854620000000, 1534597855120000000, 1534597856120000000, 1534597856620000000, 1534597857620000000,
            1534597858120000000, 1534597859120000000, 1534597859620000000, 1534597860120000000, 1534597860620000000,
            1534597861120000000, 1534597861620000000, 1534597862120000000, 1534597862620000000, 1534597863120000000,
            1534597863620000000, 1534597864120000000, 1534597864620000000, 1534597865120000000, 1534597865620000000,
            1534597866120000000, 1534597866620000000, 1534597867120000000, 1534597867620000000, 1534597868120000000,
            1534597868620000000, 1534597869120000000, 1534597869620000000, 1534597870120000000, 1534597870620000000,
            1534597871120000000, 1534597871620000000, 1534597872120000000, 1534597872620000000, 1534597873120000000,
            1534597873620000000, 1534597874120000000, 1534597874620000000, 1534597875120000000, 1534597875620000000,
            1534597876120000000, 1534597876620000000, 1534597877120000000, 1534597877620000000, 1534597878120000000,
            1534597878620000000, 1534597879120000000, 1534597879620000000, 1534597880120000000, 1534597880620000000,
            1534597881120000000, 1534597881620000000, 1534597882120000000, 1534597882620000000, 1534597883120000000,
            1534597883620000000, 1534597884120000000, 1534597884620000000, 1534597885120000000, 1534597885620000000,
            1534597886120000000, 1534597886620000000, 1534597887120000000, 1534597887620000000, 1534597888120000000,
            1534597888620000000, 1534597889120000000, 1534597889620000000, 1534597890120000000, 1534597890620000000]
        self.timestamps = [pd.Timestamp(t, tz="Europe/Zurich") for t in self.unixTimeNanoseconds]

        y = np.arange(100)
        self.dfIndexInSeconds = pd.DataFrame(y, self.unixTimeSeconds)
        self.dfIndexInNanoseconds = pd.DataFrame(y, self.unixTimeNanoseconds)
        self.dfIndexInTimestamps = pd.DataFrame(y, self.timestamps)
        self.dfSynchronized = pd.DataFrame(y, [t - self.unixTimeSeconds[0] for t in self.unixTimeSeconds])

    def tearDown(self):
        pass

    def createTestSignal(self):
        """Method creates a signal with the name test and simple dataFrame"""
        data = [1.0, 1.2, 1.4]
        now = datetime.now()

        df = pd.DataFrame(data, [0, 1, 2], columns=['value'])
        signal = Signal('test', df=df)
        return signal

    def createTestDataFrame(self):
        """Method creates a simple test DataFrame"""
        data = [1.0, 1.2, 1.4]
        now = datetime.now()
        timeIndex = pd.date_range(now, now + timedelta(len(data) - 1), freq='D')
        return pd.DataFrame(data, timeIndex, columns=['value'])

    #region write Tests

    def test_write_apiArgumentNotProvided_dbAccessNotSet_NothingIsDone(self):
        """Test that if the signals dbAccess is not set and api is not defined in a argument passed to a signal's write method
         the the _dbAccessHandler method is not called"""
        uut = Signal("test")

        with patch.object(Signal, '_databaseAccessHandler') as mock_accessHandler:

            uut.write()
            mock_accessHandler.assert_not_called()

    def test_write_apiArgumentNotProvided_dbAccessSet_correctDBAccessWriteCalled(self):
        """Test that if the signals dbAccess is set but api is not defined in a argument passed to a signal's write method,
        the dbAccess's write method is still called"""

        # arrange:
        now = datetime.now()
        kwargs = {"string": "string", "int": 1, "float": 1.5, "date": now, 'ldb': self.mockLoggingDB} # mock included because of dependency propblem
        # datetime included to test arbitrary type

        testCases = [signalPM.Signal.createSignalPM(),
                     signalCALS.Signal.createSignalCALS(ldb=self.mockLoggingDB),
                     signalInfluxDb.Signal.createSignalInflux()]

        # act:
        for behavior in testCases:
            with self.subTest(name=behavior):
                # set signal DFSignal
                self.uut.dbAccess = behavior
                with patch.object(behavior, 'write') as mock_write:
                    mock_write.return_value = True
                    # act
                    result = self.uut.write(**kwargs)

                    # assert the dbAccess write method is called right arguments
                    mock_write.assert_called_with(self.uut, string="string", int=1, float=1.5, date=now, ldb=self.mockLoggingDB)




    def test_write_apiArgumentProvided_correctDBAccessWriteCalled(self):
        """Test that the write method correctly calls the write method of the correct DFSignal type dependent on the api parameter"""

        # arrange:
        now = datetime.now()
        kwargs = {"string": "string", "int": 1, "float": 1.5, "date": now, 'ldb': self.mockLoggingDB} # mock included because of dependency propblem
        # datetime included to test arbitrary type

        testCases = [("pm", signalPM.Signal),
                     ('PM', signalPM.Signal),
                     ('cals', signalCALS.Signal),
                     ('CALS', signalCALS.Signal),
                     ('influx', signalInfluxDb.Signal),
                     ('INFLUX', signalInfluxDb.Signal)]

        # act:
        for apiKey, behavior in testCases:
            with self.subTest(name=apiKey):
                with patch.object(behavior, 'write') as mock_write:
                    mock_write.return_value = True
                    # act
                    self.uut.write(apiKey, **kwargs)

                    # assert the dbAccess write method is called right arguments
                    mock_write.assert_called_with(self.uut, string="string", int=1, float=1.5, date=now, ldb=self.mockLoggingDB)

                # assert it the dbAccess is of the right behavior type
                self.assertIsInstance(self.uut.dbAccess, behavior)

    #endregion

    #region read Tests

    def test_read_apiArgumentNotProvided_dbAccessNotSet_NothingIsDone(self):
        """Test that if the signals dbAccess is not set and api is not defined in a argument passed to a signal's read method
        the _dbAccessHandler method is not called"""
        uut = Signal("test")

        with patch.object(Signal, '_databaseAccessHandler') as mock_accessHandler:

            uut.read()
            mock_accessHandler.assert_not_called()

    def test_read_apiArgumentNotProvided_dbAccessSet_correctDBAccessReadCalled(self):
        """Test that if the signals dbAccess is set but api is not defined in a argument passed to a signal's write method,
        the dbAccess's write method is still called"""

        # arrange:
        now = datetime.now()
        kwargs = {"string": "string", "int": 1, "float": 1.5, "date": now,  'ldb': self.mockLoggingDB} # mock included because of dependency propblem
        # datetime included to test arbitrary type

        testCases = [signalPM.Signal.createSignalPM(),
                     signalCALS.Signal.createSignalCALS(ldb=self.mockLoggingDB),
                     signalInfluxDb.Signal.createSignalInflux()]

        # act:
        for behavior in testCases:
            with self.subTest(name=behavior):
                # set signal DFSignal
                self.uut.dbAccess = behavior
                with patch.object(behavior, 'read') as mock_read:
                    mock_read.return_value = self.dfIndexInNanoseconds
                    # act
                    result = self.uut.read(**kwargs)

                    # assert the dbAccess write method is called right arguments
                    mock_read.assert_called_with(self.uut, string="string", int=1, float=1.5, date=now, ldb=self.mockLoggingDB)
                    pd.testing.assert_frame_equal(self.uut.df, self.dfIndexInNanoseconds)

    def test_read_apiArgumentProvided_correctDBAccessReadCalled(self):
        """Test that read correctly call the write method of the correct DFSignal type dependent on the api parameter"""

        # arrange:
        now = datetime.now()
        kwargs = {"string": "string", "int": 1, "float": 1.5, "date": now, 'ldb': self.mockLoggingDB} # mock included because of dependency propblem
        # datetime included to test arbitrary type

        testCases = [("pm", signalPM.Signal),
                     ('PM', signalPM.Signal),
                     ('cals', signalCALS.Signal),
                     ('CALS', signalCALS.Signal),
                     ('influx', signalInfluxDb.Signal),
                     ('INFLUX', signalInfluxDb.Signal)]

        # act:
        for apiKey, behavior in testCases:
            with self.subTest(name=apiKey):
                with patch.object(behavior, 'read') as mock_read:
                    mock_read.return_value = self.dfIndexInNanoseconds
                    # act
                    self.uut.read(apiKey, **kwargs)

                    # assert the dbAccess write method is called right arguments
                    mock_read.assert_called_with(self.uut, string="string", int=1, float=1.5, date=now, ldb=self.mockLoggingDB)
                # assert it the dbAccess is of the right behavior type
                self.assertIsInstance(self.uut.dbAccess, behavior)

                pd.testing.assert_frame_equal(self.uut.df, self.dfIndexInNanoseconds)

    #endregion

    # def test_createDFSignal_setsBehaviorCorrectly(self):
    #     """Tests that createDFSignal the correct signal type when passed the string pm, cals or influx
    #     regardless of lower or capital letters are used"""
    #
    #     testCases = [("pm", signalPM.Signal),
    #                  ('PM', signalPM.Signal),
    #                  ('cals', signalCALS.Signal),
    #                  ('CALS', signalCALS.Signal),
    #                  ('influx', signalInflux.Signal),
    #                  ('INFLUX', signalInflux.Signal)]
    #
    #     for input, signalType in testCases:
    #         with self.subTest(name=input):
    #             # act
    #             signal = Signal.createDFSignal(input)
    #             # assert
    #             self.assertIsInstance(signal, signalType)


    #region constructor tests

    def test_init_NoArguments_NameAndDataFrameAreNone(self):
        """Test that when called with no arguments the signal object's name and df attribute are set as None"""
        uut = Signal()
        pd.testing.assert_frame_equal(uut.df, pd.DataFrame())

    def test_init_NameAndValidDataFrame_NameAndDataFrameSetCorrectly(self):
        """Tests that when a signal is created with a name and valid DataFrame, these attributes are set correctly"""
        df = pd.DataFrame([1.0, 1.2, 1.4], [1, 2, 3])
        uut = Signal("test", df=df)

        self.assertEqual(uut.name, "test")
        assert_frame_equal(uut.df, df)

    #endregion

    #region df property tests

    def test_dfSetter_argumentIsNotDataFrame_TypeErrorThrown(self):
        """Tests that when trying to assign df with something else than a pandas DataFrame a TypeError is thrown"""
        uut = Signal()
        testCases = [1, 1.0, "string", datetime.now()]  # datetime included to test arbitrary type

        for objectType in testCases:
            with self.subTest(name=str(objectType)):
                with self.assertRaises(TypeError):
                    uut.df = objectType

    def test_dfSetter_DataFrameIndexIsWrongType_TypeErrorThrown(self):
        """Tests that when trying to assign df with a DataFrame with wrong index type a TypeError is thrown"""
        uut = Signal()
        data = [1.0, 1.2, 1.4]
        now = datetime.now()

        testCases =[pd.DataFrame(data, [1.0, 1.2, 1.4]),
                    pd.DataFrame(data, pd.date_range(now, now + timedelta(len(data) - 1), freq='D'))]

        for df in testCases:
            with self.subTest():
                with self.assertRaises(TypeError):
                    uut.df = df

    def test_dfSetter_DataFrameIsRightType_DataFrameSetCorrectly(self):
        """Tests that when trying to assign df with a DataFrame with the right index format the assignment is made"""
        uut = Signal()
        df = pd.DataFrame([1.0, 1.2, 1.4], [1, 2, 3])
        uut.df = df

        assert_frame_equal(uut.df, df)

    #endregion

    #region dbAccess property tests
    def test_dbAccessSetter_validTypePassed_dbAccessCorrectlySet(self):
        """Tests that when provided with valid arguments, then the signal's dbAccess attribute is correctly set"""
        testCases = [
            signalPM.Signal.createSignalPM(),
            signalCALS.Signal.createSignalCALS(ldb=self.mockLoggingDB),# mock included because of dependency propblem),
            signalInfluxDb.Signal.createSignalInflux(),
        ]
        for behavior in testCases:
            with self.subTest(name=behavior):
                # act
                self.uut.dbAccess = behavior
                # assert
                self.assertIsInstance(self.uut.dbAccess, df_signal.DFSignal)

    def test_dbAccessSetter_invalidTypePassed_TypeErrorThrown(self):
        """Tests that if invalid Type is passed to setDBAccess method a TypeError is thrown"""
        with self.assertRaises(TypeError):
            self.uut.dbAccess = Signal()

    #endregion

    #region DataFrame index conversion tests

    def test_convertIndexToNanoseconds_correctConversions(self):
        """Tests that convertIndexToNanoseconds convert the index  of signal's df correctly to nanoseconds regardless of
        the current state"""
        uut = Signal("test", deepcopy(self.dfIndexInNanoseconds))

        testCases = [
            ("ns->ns", uut.nsState, deepcopy(self.unixTimeNanoseconds)),
            ("s->ns", uut.sState, deepcopy(self.unixTimeSeconds)),
            ("ts->ns", uut.tsState, deepcopy(self.timestamps))
        ]
        for name, startingState, startingIndex in testCases:
            with self.subTest(name=name):
                # arrange:
                # set starting state without using conversions:
                uut.indexState = startingState
                uut.df.set_index([startingIndex], inplace=True)

                # act:
                uut.convertIndexToNanoseconds()

                #assert:
                pd.testing.assert_frame_equal(uut.df, self.dfIndexInNanoseconds)
                self.assertIs(uut.indexState, uut.nsState)


    def test_convertIndexToSeconds_correctConversions(self):
        """Tests that convertIndexToSeconds converts the index of signal's df correctly to seconds regardless of the
        current state"""
        uut = Signal("test", deepcopy(self.dfIndexInNanoseconds))

        testCases = [
            ("ns->s", uut.nsState, deepcopy(self.unixTimeNanoseconds)),
            ("s->s", uut.sState, deepcopy(self.unixTimeSeconds)),
            ("ts->s", uut.tsState, deepcopy(self.timestamps))
        ]
        for name, startingState, startingIndex in testCases:
            with self.subTest(name=name):
                # arrange:
                # set starting state without using conversions:
                uut.indexState = startingState
                uut.df.set_index([startingIndex], inplace=True)

                # act:
                uut.convertIndexToSeconds()

                #assert:
                pd.testing.assert_frame_equal(uut.df, self.dfIndexInSeconds)
                self.assertIs(uut.indexState, uut.sState)

    def test_convertIndexToTimestamps_correctConversions(self):
        """Tests that convertIndexToTimestamps convert the index  of signal's df correctly to Timestamp objects
        regardless of the current state"""
        uut = Signal("test", deepcopy(self.dfIndexInNanoseconds))

        testCases = [
            ("ns->ts", uut.nsState, deepcopy(self.unixTimeNanoseconds)),
            ("s->ts", uut.sState, deepcopy(self.unixTimeSeconds)),
            ("ts->ts", uut.tsState, deepcopy(self.timestamps))
        ]
        for name, startingState, startingIndex in testCases:
            with self.subTest(name=name):
                # arrange:
                # set starting state without using conversions:
                uut.indexState = startingState
                uut.df.set_index([startingIndex], inplace=True)

                # act:
                uut.convertIndexToTimestamps()

                #assert:
                pd.testing.assert_frame_equal(uut.df, self.dfIndexInTimestamps)
                self.assertIs(uut.indexState, uut.tsState)


    def test_convertIndex_multipleConversions_correctEndState(self):
        """Tests that the indexState of a signal is tracked correctly even when multiple conversion calls are made"""
        uut = Signal("test", deepcopy(self.dfIndexInNanoseconds))

        self.assertIs(uut.indexState, uut.nsState)

        #act :
        uut.convertIndexToSeconds()
        uut.convertIndexToTimestamps()
        uut.convertIndexToSeconds()
        uut.convertIndexToNanoseconds()
        uut.convertIndexToTimestamps()
        uut.convertIndexToSeconds()

        # assert:
        pd.testing.assert_frame_equal(uut.df, self.dfIndexInSeconds)
        self.assertIs(uut.indexState, uut.sState)

    def test_getsynchronizedDF_returnsSynchronizedDataFrame_WithoutAlteringIndexState(self):
        """Tests that getSynchronizedDF returns a DataFrame where the time axis has been synchronized and that the indexState
        is unchanged after the call"""

        uut = Signal("test", deepcopy(self.dfIndexInNanoseconds))

        testCases = [
            ("ns", uut.nsState, deepcopy(self.unixTimeNanoseconds)),
            ("s", uut.sState, deepcopy(self.unixTimeSeconds)),
            ("ts", uut.tsState, deepcopy(self.timestamps))
        ]
        for name, startingState, startingIndex in testCases:
            with self.subTest(name=name):
                # arrange:
                # set starting state without using conversions:
                uut.indexState = startingState
                uut.df.set_index([startingIndex], inplace=True)

                # act:
                result = uut.getSynchronizedDF()

                # assert:
                pd.testing.assert_frame_equal(result, self.dfSynchronized)
                self.assertIs(uut.indexState, startingState)

    def test_convertIndexToMethods_calledWithUnset_dfproperty_doesNoting(self):
        """test that indexState's convertToState method is not called if signal's df is None for all conversion methods"""
        uut = Signal("test")

        testCases = [uut.convertIndexToNanoseconds, uut.convertIndexToSeconds, uut.convertIndexToTimestamps,
                     uut.getSynchronizedDF]

        for testMethod in testCases:
            with self.subTest():

                with patch.object(DataFrameIndexState, 'convertToState') as mock_convertToState:

                    result = testMethod()

                    mock_convertToState.assert_not_called()

                    self.assertIsNone(result)


    #endregion