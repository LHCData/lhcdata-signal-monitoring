import requests
import json
import numpy as np
from datetime import timedelta, date

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)
        
def get_PM_event(system, className, source, eventTimeStampInNanos, eventDurationInNanos = 0, HeaderOnly = False):
    if eventDurationInNanos > 0:
        if HeaderOnly:
            request = "http://pm-api-pro/v2/pmevent/header/within/duration?system="\
            +system+"&className="+className+"&source="+source+"&fromTimestampInNanos="\
            +str(eventTimeStampInNanos)+"&durationInNanos="+str(eventDurationInNanos)
        else:
            request = "http://pm-api-pro/v2/pmevent/within/duration?system="\
            +system+"&className="+className+"&source="+source+"&fromTimestampInNanos="\
            +str(eventTimeStampInNanos)+"&durationInNanos="+str(eventDurationInNanos)
    else:
        if HeaderOnly:
            request = "http://pm-api-pro/v2/pmevent/header?system="\
            +system+"&className="+className+"&source="+source+"&TimestampInNanos="+str(eventTimeStampInNanos)
        else:
            request = "http://pm-api-pro/v2/pmevent?system="\
            +system+"&className="+className+"&source="+source+"&TimestampInNanos="+str(eventTimeStampInNanos)
    response = requests.get(request)
    try:
        PM_data = json.loads(response.text)
    except:
        PM_data = ''
    return PM_data

def get_PM_data(system, className, source, eventTimeStampInNanos, eventDurationInNanos = 0, HeaderOnly = False):
    if eventDurationInNanos > 0:
        if HeaderOnly:
            request = "http://pm-api-pro/v2/pmdata/header/within/duration?system="\
            +system+"&className="+className+"&source="+source+"&fromTimestampInNanos="\
            +str(eventTimeStampInNanos)+"&durationInNanos="+str(eventDurationInNanos)
        else:
            request = "http://pm-api-pro/v2/pmdata/within/duration?system="\
            +system+"&className="+className+"&source="+source+"&fromTimestampInNanos="\
            +str(eventTimeStampInNanos)+"&durationInNanos="+str(eventDurationInNanos)
    else:
        if HeaderOnly:
            request = "http://pm-api-pro/v2/pmdata/header?system="\
            +system+"&className="+className+"&source="+source+"&TimestampInNanos="+str(eventTimeStampInNanos)
        else:
            request = "http://pm-api-pro/v2/pmdata?system="\
            +system+"&className="+className+"&source="+source+"&TimestampInNanos="+str(eventTimeStampInNanos)
        
    response = requests.get(request)
    try:
        PM_data = json.loads(response.text)
    except:
        PM_data = ''
    return PM_data

def get_PM_signal(system, className, source, eventTimeStampInNanos, signal):
    request = "http://pm-api-pro/v2/pmdata/signal?system="\
    +system+"&className="+className+"&source="+source+"&timestampInNanos="+str(eventTimeStampInNanos)+"&signal="+signal
    print(request)
    response = requests.get(request)
    try:
        PM_data = json.loads(response.text)
    except:
        PM_data = ''
    return PM_data

def findPmEvents(eventTimeStampInNanos, eventDurationSeconds, source, system, className, magnetType = ""):
    eventList = []
    eventDurationInNanos = int(eventDurationSeconds*1e9)
        
    # Fetch PM event data
    pmHeaders = get_PM_data(system, className, source, eventTimeStampInNanos, eventDurationInNanos, True)

    for pmHeader in pmHeaders:
        if 'DT' not in pmHeader['source']:
            if magnetType == "":
                eventList.append((pmHeader['source'], pmHeader['timestamp']))
            else:
                eventList.append((magnetType, pmHeader['source'], pmHeader['timestamp']))

        # if 'DT' in 'source', then rubbish data injected into official PM data
        
    return eventList


def getWindowWithGivenMeanAndStd(originSeries, indexIncrement=20, mean=50, std=0.1):
    for j in range(len(originSeries) - indexIncrement):
        window = originSeries.iloc[j:j + indexIncrement]
        windowMean = window.mean()
        windowStd = window.std()
        isWindowFound = windowMean >= mean and windowStd <= std
        
        if isWindowFound:
            minIndex = originSeries.iloc[j:].idxmin() # find the time index of minimum value
            minIndex = originSeries.index.get_loc(minIndex) # find the iteration of this index
            return originSeries.iloc[j:minIndex].copy()
    
    # throw exception if not found
    raise Error("No window found")
    
def calculateTimeConstantOfExponentialDecayInSeconds(exponentialDecayInSeconds):
    return 2*np.trapz(exponentialDecayInSeconds, x=exponentialDecayInSeconds.index, axis=0) / (exponentialDecayInSeconds.iloc[0] - exponentialDecayInSeconds.iloc[-1])
    
def getDecayTimeConstant(signalDFInSeconds, timeUnitMultiplier=1000):
    sliceDF = getWindowWithGivenMeanAndStd(signalDFInSeconds)  
    sliceSquaredDF = sliceDF.pow(2)
    return timeUnitMultiplier * calculateTimeConstantOfExponentialDecayInSeconds(sliceSquaredDF)
    

