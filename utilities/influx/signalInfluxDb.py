from utilities.signal.df_signal import DFSignal
from influxdb import DataFrameClient
from utilities.signal.dataFrameIndexConverter import DataFrameIndexConverter


class Signal(DFSignal):
    """Class for encapsulating reading and writing methods of the Influx API"""

    def __init__(self, client):
        super().__init__('Influx')
        self.client = client

    @staticmethod
    def createDataFrameClient(username='', password='', host='dbod-lhc-sm.cern.ch', port=8081, database='test',ssl=True):
        """"Method creates and returns a DataFrameClient"""
        return DataFrameClient(host, port, username, password, database, ssl)

    @staticmethod
    def createSignalInflux(**kwargs):
        """Method validates kwargs to create and return a Signal object.
        The method looks though kwargs for arguments to create a new DataFrameClient. If kwargs contains the necessary
        arguments, a client is created. If kwargs contain a DataFrameClient object, this is used instead.
        If neither a complete client or parameters to build one is provided, a default client is created.
        The resulting client is used to create a return a Signal object"""
        client = None

        username = Signal.validateKeyInkwargs('username', str, **kwargs)
        password = Signal.validateKeyInkwargs('password', str, **kwargs)
        host = Signal.validateKeyInkwargs('host', str, **kwargs)
        port = Signal.validateKeyInkwargs('port', int, **kwargs)
        database = Signal.validateKeyInkwargs('database', str, **kwargs)

        if username and password and host and port and database:
            client = Signal.createDataFrameClient(username, password, host, port, database)
        else:
            client = Signal.createDataFrameClient('root', 'root')

        client = Signal.validateKeyInkwargs("client", DataFrameClient, **kwargs) or client
        return Signal(client)

    def write(self, signal=None, **kwargs):
        """Method writes the DataFrame contained in signal to the database specified in the signal instance's client field.
        Returns True if success

        kwargs:
        -----------------------------------------------
        tags:
            Can be used to pass a tag set defined as a dictionary to the database write_points call.
        protocol:
            Can be used to set the protocol for the database write call. Default to 'json'
        """
        signal.convertIndexToTimestamps()
       
        tags = {}
        if 'tags' in kwargs:
            tags = kwargs['tags']

        if 'protocol' in kwargs:
            protocol = kwargs['protocol']
        else:
            protocol = 'json'

        return self.client.write_points(dataframe=signal.df,
                                        measurement=signal.name,
                                        tags=tags,
                                        protocol=protocol,
                                        time_precision='n')

    def read(self, signal, **kwargs):
        """
        PRECONDITION: A Signal instance has been created.
        POSTCONDITION: The df property of the signal object has been set to the query result, and this result is 
        returned as well.

        DESCRIPTION:
        Method reads data from database specified in the signal instance's client field. The hierarchy of the query is 1: query passed as parameter. 2: if signalName param is given
        this will be queried and overwrite this instance's own name field. 3) Are none of these parameters given, it is assumed that the signal name are already set.
        This method also returns the resulting dataFrame."""""

        signalName = self.validateKeyInkwargs("signalName", str, **kwargs)
        query = self.validateKeyInkwargs("query", str, **kwargs)

        if signalName:
            signal.name = signalName
        queryString = 'select * from {0}'.format(signal.name)

        if query and signalName:
            queryString = query
        
        print(queryString)
        queryResult = self.client.query(queryString)
        df = queryResult.get(signal.name)
        if df is not None:
            DataFrameIndexConverter.convertIndexFromTimestampsToNanoseconds(df)

        return df