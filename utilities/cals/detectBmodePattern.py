import os
import sys
from pathlib import Path

curr_dir = Path(os.path.split(os.getcwd())[0])
utilities_dir = str(curr_dir.parent / 'utilities')

if utilities_dir not in sys.path:
	sys.path.append(utilities_dir)

import pandas as pd
import numpy as np
import cals.signalCALS as scals


class DetectBmodePattern(object):

	def __init__(self, startTime={}, endTime={}, bMode={}, trigger={}):
		self.StartTime = startTime
		self.EndTime = endTime
		self.BMode = bMode
		self.Trigger = trigger

	def Stamp(self):
		""" Function defines triggers & give back timestamps before and after ramp """
		# look for the whole BMode Signal in given range
		bModeDF = scals.Signal(self.BMode, self.StartTime, self.EndTime).getSignal(self.BMode)

		# It is possible to define different triggers values
		if self.Trigger == {}: self.Trigger = [2.0, 6.0, 8.0, 14.0]
		# only takes given triggers from BMode
		bModeDF = bModeDF[bModeDF[self.BMode].isin(self.Trigger)]

		triggerDF = self.Filter(bModeDF)

		TriggerLowStamp = triggerDF.iloc[1].values  # values @6.0
		TriggerHighStamp = triggerDF.iloc[2].values  # values @8.0

		return [TriggerLowStamp, TriggerHighStamp]

	def Filter(self, bModeDF):
		""" Function filters out ramps which are to short or incomplete"""
		triggerDF = pd.DataFrame(data=np.zeros(len(self.Trigger)), index=self.Trigger)
		i = 1
		toShort = 0
		incomplete = 0
		# only take signals wich have all bmodes in the right order in it
		for b in range(4, len(bModeDF[self.BMode])):
			if all([bModeDF[self.BMode].iloc[b - 3 + t] == self.Trigger[t] for t in range(len(self.Trigger))]):
				if (bModeDF[self.BMode].index[b - 2] - bModeDF[self.BMode].index[b - 3] >= 1800) and \
						(bModeDF[self.BMode].index[b] - bModeDF[self.BMode].index[b - 1] >= 1800):
					newFrame = pd.DataFrame(bModeDF[self.BMode].index[b - 3:b + 1], index=self.Trigger)
					newFrame.columns = [i]
					i += 1
					triggerDF = pd.concat([triggerDF, newFrame], axis=1, join='inner')
				else:
					toShort += 1
			else:
				incomplete += 1

		incomplete = incomplete - (i - 2) * 4
		print(str(incomplete) + " triggers have been skiped because of incompleteness")
		print(str(toShort) + " ramps have not been taken because of to short up/down time")
		del triggerDF[0]

		return triggerDF
