import pandas as pd

class DataFrameIndexConverter(object):
    """class for encapsulating converting the index of a pandas DataFrame"""

    @staticmethod
    def convertDataFrameIndex(dataFrame, convertUnit):
        """Method applies a conversion function to the index of the signal's DataFrame"""
        index = dataFrame.index.tolist()
        newIndex = [convertUnit(t) for t in index]
        dataFrame.set_index([newIndex], inplace=True)

    @staticmethod
    def convertIndexFromNanosecondsToSeconds(dataFrame):
        """Method converts the index of a signal passed DataFrame to unix time seconds assuming current index is
        unix time ns"""
        DataFrameIndexConverter.convertDataFrameIndex(dataFrame, lambda ns: ns * 1e-9)

    @staticmethod
    def convertIndexFromSecondsToNanoseconds(dataFrame):
        """Method converts the index of a passed DataFrame to unix time ns assuming current index is
        unix time seconds"""
        DataFrameIndexConverter.convertDataFrameIndex(dataFrame, lambda s: int(s * 1e9))

    @staticmethod
    def convertIndexFromNanosecondsToTimestamps(dataFrame):
        """Method converts the index of a passed DataFrame to pandas timestamps assuming current index is
        unix time ns"""
        DataFrameIndexConverter.convertDataFrameIndex(dataFrame, lambda ns: pd.Timestamp(ns, tz="Europe/Zurich"))

    @staticmethod
    def convertIndexFromTimestampsToNanoseconds(dataFrame):
        """Method converts the index of a passed DataFrame to unix time ns assuming current index is
        pandas timestamps"""
        DataFrameIndexConverter.convertDataFrameIndex(dataFrame, lambda ts: ts.value)
