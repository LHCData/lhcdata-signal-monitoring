import datetime


def unixTimeInNanoSecond2datatime(inputTimeStampInNanoSecond):
	""" Function converts from the unix time in ns to a date time object """
	format_str = "%Y-%m-%d %H:%M:%S.%M"
	return datetime.datetime.fromtimestamp(inputTimeStampInNanoSecond / 1e9).strftime(format_str)


def datetime2unixtimeInSecond(inputDatetime):
	""" Function converts from the input date time as a datetime object to ns """
	return (inputDatetime - datetime.datetime(1970, 1, 1)).total_seconds()


def datetime2unixtimeInNanoSecond(inputDatetime):
	""" Function converts from the input date time as a datetime object to second """
	return (inputDatetime - datetime.datetime(1970, 1, 1)).total_seconds() * 1e9


def timestamp2unixtimeInNanoSecond(inputDatetime):
	""" Function converts from the input date time as a timestamp object to ns """
	return inputDatetime.value

#
# def unixTimeInNanoSecond2timestampString(inputTimeStampInNanoSecond):
# 	""" Function converts from the unix time in ns to a string represent the timestamp with us precision"""
# 	#format_str = "%Y-%m-%d %H:%M:%S.%M"
# 	format_str = "%Y-%m-%d %H:%M:%S.%f"
# 	return datetime.datetime.fromtimestamp(inputTimeStampInNanoSecond / 1e9).strftime(format_str)
#
# def unixTimeInNanoSeconds2dateTime(inputTimeStampInNanoSeconds):
# 	""" Function converts from the unix time in ns to a dateTime object"""
# 	return datetime.datetime.fromtimestamp(inputTimeStampInNanoSeconds / 1e9)
#
