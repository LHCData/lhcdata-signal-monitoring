import os
import sys
from pathlib import Path

curr_dir = Path(os.path.split(os.getcwd())[0])
utilities_dir = str(curr_dir.parent / 'utilities')

if utilities_dir not in sys.path:
	sys.path.append(utilities_dir)

import pandas as pd
import utils as u
import similarity as sim
import preprocessing as pre

import cals.detectBmodePattern as d

# https://github.com/rdemaria/pytimber 123
import pytimber

ldb = pytimber.LoggingDB()

# MyFunctions:
import cals.signalCALS as scals


# provide signal name ,start time , end time , beam mode, sector(tuple)
def voltage_feeler(signalName, t1, t2, bMode, sector):
	voltages_right = ldb.search('%R' + str(min(sector)) + '%EARTH_RB')
	voltages_left = ldb.search('%L' + str(max(sector)) + '%EARTH_RB')
	del voltages_left[0]
	voltages = voltages_left + voltages_right

	[low, high] = d.DetectBmodePattern(t1, t2, bMode).Stamp()

	startTimeFirst = u.unixTimeInNanoSecond2datatime(low[0] * 1e9)
	endTimeFirst = u.unixTimeInNanoSecond2datatime(high[0] * 1e9)

	startTimeSecond = u.unixTimeInNanoSecond2datatime(low[1] * 1e9)
	endTimeSecond = u.unixTimeInNanoSecond2datatime(high[1] * 1e9)

	signalCALSFirst = scals.Signal(signalName, startTimeFirst, endTimeFirst).getSignal(signalName)
	signalCALSSecond = scals.Signal(signalName, startTimeSecond, endTimeSecond).getSignal(signalName)

	FirstRamp = scals.Signal(voltages, startTimeFirst, endTimeFirst).getSignalDF()
	SecondRamp = scals.Signal(voltages, startTimeSecond, endTimeSecond).getSignalDF()

	CostDTW = []
	CostL1 = []
	CostL2 = []
	CostLinf = []

	for i in voltages:
		# First and second ramp:
		FirstSignal = FirstRamp[i]
		SecondSignal = SecondRamp[i]

		resampledFirst, resampledSecond = pre.Pre_Processing(FirstSignal, SecondSignal, 1)

		s1 = resampledFirst.values
		s2 = resampledSecond.values

		CostL1.append(sim.L1Distance(s1, s2))
		CostL2.append(sim.L2Distance(s1, s2))
		CostLinf.append(sim.LinfDistance(s1, s2))
		CostDTW.append(sim.DTWDistance(s1, s2))

	m = {'L1': CostL1, 'L2': CostL2, 'LINF': CostLinf, 'DTW': CostDTW}
	Results = pd.DataFrame(data=m, index=voltages)
	return Results
