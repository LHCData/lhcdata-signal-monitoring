import os
import sys
from pathlib import Path

curr_dir = Path(os.path.split(os.getcwd())[0])
utilities_dir = str(curr_dir.parent / 'utilities')

if utilities_dir not in sys.path:
	sys.path.append(utilities_dir)

import numpy as np

# matplotlib imports
import matplotlib.gridspec as gridspec

import matplotlib.pyplot as plt

# scipy imports

# ipywidgets imports

# self made functions
import editDF as edf
import cals.filterToolbox as ftb
import cals.signalAnalysis as sa

# set global variables for history
log = []
warnings = []


class Dashboard():

	def enableTimestampPlotting(resistance):
		""" This is necessary because of an error in the matplotlib.pyplot library """
		resistance['2010-03-14 12:14:14.14'].plot(style='o', markersize=3, figsize=(0.1, 0.1))

	def showDash(Time, Location, bigSignals, resistance, timeParameter, locationParameter, filterText):
		""" Function creates Dashboard """
		# Dashboard-SETTINGS
		gs = gridspec.GridSpec(3, 15)
		gs.update(wspace=0, hspace=0.3)
		fig, plot = plt.subplots(figsize=(25, 15))
		ymargin = 1.5e-8  # for y_axis

		# TIME
		plt.subplot(gs[0, 0:8])
		k = Dashboard.timePlot(bigSignals, Location, ymargin)
		# TIME Histogram
		plt.subplot(gs[0, 8:10])
		muV, sigmaV = Dashboard.timeHist(bigSignals, Location, ymargin)

		# LOCATION
		plt.subplot(gs[1, 0:8])
		currentSector = Dashboard.locationPlot(resistance, bigSignals, Time, Location, ymargin)
		# LOCATION Histogram
		plt.subplot(gs[1, 8:10])
		muS, sigmaS = Dashboard.locationHist(currentSector, ymargin)

		# ORIGIN
		plt.subplot(gs[2, 0:8])
		snr, currentSNRlow, currentSNRhigh, percentage = Dashboard.originPlot(resistance, bigSignals, Time,
		                                                                      timeParameter, Location, ymargin)

		# History
		plt.subplot(gs[0:3, 10:13])
		# Generate Log-Text
		logText = Dashboard.generateLogText(Time, Location, bigSignals, muV, sigmaV, muS, sigmaS, percentage)
		log.append(filterText + logText)
		warnText = Dashboard.generateWarnText(k, snr, currentSNRlow, currentSNRhigh)
		warnings.append(warnText)
		Dashboard.historyPlot(log, warnings)

	def timePlot(bigSignals, Location, ymargin):
		""" Function plots values over time """
		date = edf.EditDF.time(bigSignals, 'date')  # Array with dates
		x = edf.EditDF.time(bigSignals, 'years')  # Array with time in years

		plt.ylabel('$R_{busbar} [\Omega]$')
		plt.xlabel('Time [years]')
		plt.title('$R_{busbar}  = f(t)$  @' + Location)
		plt.ylim((0, ymargin))
		cmap = plt.get_cmap("winter")

		# plot all points from filtered Dataframe
		y = bigSignals.values
		for i in range(0, len(y)):
			plt.plot(date, y[i], 'o', markersize=1, color=cmap(int(i * (256 / len(y)))))  # cmap has 256 colors

		# plot selected location in red
		loc = bigSignals.index.get_loc(Location)  # loc is index possition
		plt.plot(date, y[loc], 'o', markersize=2, color='r')

		# Linear regression of resitance
		reg, k = sa.SignalAnalysis.linReg(bigSignals, Location)
		plt.plot(date, reg, 'r--')
		plt.gcf().autofmt_xdate()

		plt.xticks(rotation='horizontal', ha='left')

		return k

	def timeHist(bigSignals, Location, ymargin):
		""" Function creates histogram of chosen location """
		v = bigSignals.loc[Location].values
		v = sa.SignalAnalysis.rejectOutliers(v, 2)  # How much outliers should we reject?

		plt.title('Histogram Time')
		plt.hist(v, 100, range=[0, ymargin], normed=1, align='mid', orientation='horizontal', color='r')
		muV = np.nanmean(v)
		sigmaV = np.nanstd(v)  # for Parameter-Log

		frame1 = plt.gca()
		plt.xticks([], [])
		frame1.axes.yaxis.set_ticklabels([])
		frame1.spines["top"].set_visible(False)
		frame1.spines["right"].set_visible(False)
		frame1.spines["bottom"].set_visible(False)
		return muV, sigmaV

	def locationPlot(resistance, bigSignals, Time, Location, ymargin):
		""" Function plots values over sectors """
		plt.ylim((0, ymargin))
		s = ['12', '23', '34', '45', '56', '67', '78', '81']
		# Memory allocation
		currentSector = {}
		x = {}
		ticks = np.zeros(len(s) + 1)
		ticks[0] = 0
		(muS, sigmaS) = (0, 0)
		for i in range(len(s)):
			sector = ftb.FilterToolbox.sector(resistance, s[i])
			offset = 20
			ticks[i + 1] = ticks[i] + len(sector) + offset
			x = np.arange(ticks[i] + offset / 2, ticks[i] + offset / 2 + sector.shape[0])
			Y = sector[Time].values
			# plot all
			plt.bar(x, Y, width=1, color='SkyBlue')
			for i in range(len(sector.index)):
				# plot filtered locations
				# for vi in range(len(bigSignals.index.values)):
				# j=0
				# if sector.index[i] == bigSignals.index.values[vi]:
				# vindex = bigSignals.index.values[vi]
				# v = bigSignals.loc[vindex].values[j]
				# j +=1
				# plt.bar(x[i], v, width = 1, color='orange')
				if sector.index[i] == Location:
					# plot sector
					plt.bar(x, Y, width=1, color='b')
					# plot chosen Signal
					plt.bar(x[i], Y[i], width=3, color='r')
					currentSector = Y
		plt.ylabel('$R_{busbar} [\Omega]$')
		plt.xlabel('Location [sector]')
		plt.title(r"$R_{busbar} = f(location)$  @" + Time)

		xticklabels = np.arange(len(s) + 1)
		xticklabels[0] = 8
		plt.xticks(ticks, xticklabels)
		return currentSector

	def locationHist(currentSector, ymargin):
		""" Function creates histogram of sector in which the signal is in """
		plt.hist(currentSector, 100, range=[0, ymargin], normed=1, align='mid', orientation='horizontal', color='b')
		muS = np.nanmean(currentSector)
		sigmaS = np.nanstd(currentSector)

		plt.title('Histogram Sector')
		frame1 = plt.gca()
		plt.xticks([], [])
		frame1.axes.yaxis.set_ticklabels([])
		frame1.spines["top"].set_visible(False)
		frame1.spines["right"].set_visible(False)
		frame1.spines["bottom"].set_visible(False)
		return muS, sigmaS

	def originPlot(resistance, bigSignals, Time, timeParameter, Location, ymargin):
		""" Function plots voltage and current which were taken to calculate the resistance """
		plt.title('$U_{RES}=f (t)$  $I_{MEAS}=f (t)$  @' + Time + ' @' + Location)
		plt.xlabel(r"Time [min]")
		plt.ylabel('Voltage [V]')
		# get full ramp, lower part of ramp and higher part of ramp
		(vDF, lowVoltage, highVoltage) = sa.SignalAnalysis.getSignalParts(timeParameter, Time, Location)
		vIndex = vDF.index.values - vDF.index[0]  # index starts with 0

		plt.plot(vIndex / 60, vDF.values, 'o', markersize=0.2)
		plt.plot(vIndex[0:len(lowVoltage.values)] / 60, lowVoltage.values, 'bo', markersize=0.5)
		plt.plot(vIndex[(len(vIndex) - len(highVoltage.values)):len(vIndex)] / 60, highVoltage.values, 'bo',
		         markersize=0.5)

		currentName = ftb.FilterToolbox.matchCurrent(Location)
		# get full ramp, lower part of ramp and higher part of ramp
		(iDF, lowCurrent, highCurrent) = sa.SignalAnalysis.getSignalParts(timeParameter, Time, currentName)
		iIndex = iDF.index.values - iDF.index[0]  # index starts with 0

		p = plt.twinx()
		p.plot(iIndex / 60, iDF.values, 'o', markersize=1, color='orange')
		p.plot(iIndex[0:len(lowCurrent.values)] / 60, lowCurrent.values, 'o', markersize=3, color='r')
		p.plot(iIndex[(len(iIndex) - len(highCurrent.values)):len(iIndex)] / 60, highCurrent.values, 'o', markersize=3,
		       color='r')
		p.set_ylabel('Current [A]')

		# Calculate parameters
		snr = sa.SignalAnalysis.snr(lowVoltage.values)  # lower part of the voltage has lower SNR
		currentSNRlow = sa.SignalAnalysis.snr(lowCurrent.values)
		currentSNRhigh = sa.SignalAnalysis.snr(highCurrent.values)
		percentage = sa.SignalAnalysis.percentage(len(bigSignals.index.values), len(resistance.index.values))

		return (snr, currentSNRlow, currentSNRhigh, percentage)

	def historyPlot(log, warnings):
		""" Function plots the history of the previous selections """
		# Creating figure and axis
		plt.gca().set_xlim(0., 1.)
		plt.gca().set_ylim(0., 1.)
		plt.gca().set_title("History")  # , weight='bold'
		plt.xticks([], [])
		plt.yticks([], [])
		# Write into Log
		n_lines = 3
		# Gap between lines in axes coords
		line_axesfrac = (1. / (n_lines))
		# shift, once the log is full
		if len(log) > n_lines:
			shift = len(log) - n_lines
		else:
			shift = 1
		for i_line in range(shift, n_lines + shift):
			baseline = 1 - (i_line - shift) * line_axesfrac
			baseline_next = baseline - line_axesfrac
			fill_color = ['grey', 'white'][i_line % 2]
			plt.fill_between([0., 1.], [baseline, baseline],
			                 [baseline_next, baseline_next],
			                 color=fill_color, alpha=0.15)
			if i_line < len(log):
				plt.annotate(log[i_line],
				             xy=(0.03, baseline - 0.8 * line_axesfrac),
				             xycoords='data', fontsize=9.5)
				plt.annotate(warnings[i_line],
				             xy=(0.03, baseline - 0.95 * line_axesfrac),
				             xycoords='data', color='r', fontsize=9.5)
			else:
				plt.annotate('',
				             xy=(0.07, baseline - 0.3 * line_axesfrac),
				             xycoords='data')

	def generateLogText(Time, Location, bigSignals, muV, sigmaV, muS, sigmaS, percentage):
		""" function generates the Log-Text """
		logText = '\n' + '@' + Time + ' @' + Location \
		          + '\n' + "$R_{busbar}=$" + str(bigSignals.loc[Location][Time]) + "$\Omega$" \
		          + '\n' + 'Histogram data:' \
		          + '\n' + '    Time: ' + r"$\mu$= " + str(muV)[0:5] + str(muV)[-4:len(
			str(muV))] + "$\Omega$   " + "$\sigma$=" + str(sigmaV)[0:5] + str(sigmaV)[-4:len(str(sigmaV))] + "$\Omega$" \
		          + '\n' + '    Sector: ' + r"$\mu$= " + str(muS)[0:5] + str(muS)[-4:len(
			str(muS))] + "$\Omega$   " + "$\sigma$=" + str(sigmaS)[0:5] + str(sigmaS)[-4:len(
			str(sigmaV))] + "$\Omega$" + '\n' + '\n'
		return logText

	def generateWarnText(k, snr, currentSNRlow, currentSNRhigh):
		""" function generates the Warning-Text"""
		warnText = ''
		if k > 0 or k < -5e-10 or snr < 0.1 or currentSNRlow < 100 or currentSNRhigh < 100:
			warnText = 'Warnings:'
		if k > 0:
			warnText += '\n' + 'Derivative is positive: ' + str(k) + r" $\frac{ \Omega\ }{year}$"
		elif k < -5e-10:
			warnText += '\n' + 'Splices could have been changed over time'
		if snr < 0.1:
			warnText += '\n' + 'Voltage SNR is very small: ' + str(snr)
		if currentSNRlow < 100 or currentSNRhigh < 100:
			warnText += '\n' + 'Inappropriate ramp for resistance calculation'
		return warnText

	def listToTXT(log, name):
		""" function writes list to txt """
		history = open(name + '.txt', 'w')
		for item in log:
			history.write("%s\n" % item)
