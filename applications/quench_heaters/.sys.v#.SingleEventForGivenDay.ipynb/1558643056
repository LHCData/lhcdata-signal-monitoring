{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import sys\n",
    "from pathlib import Path\n",
    "\n",
    "curr_dir = Path(os.path.split(os.getcwd())[0])\n",
    "utilities_dir = str(curr_dir.parent)\n",
    "\n",
    "if utilities_dir not in sys.path:\n",
    "    sys.path.append(utilities_dir)\n",
    "\n",
    "import requests\n",
    "import json\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import scipy as sp\n",
    "from scipy import signal\n",
    "import utilities.utils as u\n",
    "import matplotlib as mpl\n",
    "from utilities.signal.signal import Signal\n",
    "from datetime import timedelta, date\n",
    "from metadata import QhPmMetadata as qh\n",
    "from metadata import RbMetadata as rb\n",
    "\n",
    "import pmutils as pmu\n",
    "import re\n",
    "import csv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Choose magnet type and day to analyse"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eventTimeStamp = '2015-03-13 00:00:00.000'\n",
    "magnetTypes = ['MB']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Get events for a single day - comparison with the Java PM Browser\n",
    "<img src = \"JavaPMBrowser.png\" width=50%>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "eventDurationSeconds = 24*60*60 # 1 day in seconds (max duration allowed in request)\n",
    "\n",
    "eventTimeStampInNanos = u.timestamp2unixtimeInNanoSecond(pd.Timestamp(eventTimeStamp, tz='Europe/Zurich'))\n",
    "magnetTypeToSignalMetadata = qh.getMetadata(magnetTypes)\n",
    "eventSourceAndTimestamp = []\n",
    "for magnetType, signalMetadata in magnetTypeToSignalMetadata.items(): # for all requested magnet types\n",
    "    eventSourceAndTimestamp += pmu.findPmEvents(eventTimeStampInNanos, eventDurationSeconds,\\\n",
    "                                           qh.source, qh.system, signalMetadata['className'], magnetType)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Display number of events for a single day"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Total events on {}: {}\".format(eventTimeStamp, len(eventSourceAndTimestamp)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Display event source and timestamp of a single day"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for magnetType, event, timestamp in eventSourceAndTimestamp:\n",
    "    print('Analysing quench heater firing of {} on {}, {}'\\\n",
    "          .format(event, u.unixTimeInNanoSecond2datatime(timestamp), timestamp))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Choose an event to be analysed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eventIndex = 0\n",
    "magnetType, magnet, timestamp = eventSourceAndTimestamp[eventIndex]\n",
    "metadata = qh.getMetadata([magnetType])[magnetType]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Process single event of a single day - get raw data\n",
    "- get all voltages\n",
    "- get all currents\n",
    "\n",
    "Assumptions: A single event is always for a single magnet type and a single magnet name\n",
    "\n",
    "Inputs: subDictionary for one magnet type, system, className, source, magnetName, eventTimeStamp\n",
    "\n",
    "Output: DF"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "system = qh.system\n",
    "className = metadata['className']\n",
    "uNames = metadata['uNames']\n",
    "iNames = metadata['iNames']\n",
    "\n",
    "QhDischarge = Signal.getPmSignals(uNames+iNames, system, className, magnet, timestamp)\n",
    "myDF = QhDischarge.getSynchronizedDF()\n",
    "myDF.filter(regex=(\"U_HDS_\\d{1}\")).plot(figsize=(5,5))\n",
    "myDF.filter(regex=(\"I_HDS_\\d{1}\")).plot(figsize=(5,5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Compare raw data from REST API to Java PM Browser\n",
    "<img src = \"JavaPMBrowserVoltage.png\" width=50%>\n",
    "<img src = \"JavaPMBrowserCurrent.png\" width=50%>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Process single event of a single day - filter signals\n",
    "- filter all signals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Signal.filterSignalNames(QhDischarge.df, metadata['filterNames'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Process single event of a single day - calculate features as a table\n",
    "- calculate features: min value, max value, mean value, std value, start value, end value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "featureDF = Signal.getFeatureDataFrame(QhDischarge.df)\n",
    "featureDF"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Process single event of a single day - calculate features as a vector\n",
    "- calculate features: min value, max value, mean value, std value, start value, end value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "featureDF = QhDischarge.df\n",
    "\n",
    "columns = ['{}_max'.format(a) for a in featureDF.columns]\\\n",
    "        + ['{}_min'.format(a) for a in featureDF.columns]\\\n",
    "        + ['{}_avg'.format(a) for a in featureDF.columns]\\\n",
    "        + ['{}_std'.format(a) for a in featureDF.columns]\\\n",
    "        + ['{}_0'.format(a) for a in featureDF.columns]\\\n",
    "        + ['{}_end'.format(a) for a in featureDF.columns]\n",
    "\n",
    "df = pd.DataFrame(columns=columns)\n",
    "print(type(featureDF.max().values))\n",
    "values = np.append(featureDF.max().values, featureDF.max().values)\n",
    "values = np.append(values, featureDF.mean().values)\n",
    "values = np.append(values, featureDF.std().values)\n",
    "values = np.append(values, featureDF.iloc[0])\n",
    "values = np.append(values, featureDF.iloc[-1])\n",
    "\n",
    "df.loc[0] = values\n",
    "df.index = [timestamp]\n",
    "df.index.name = 'timestamp'\n",
    "df.to_csv('PmEventFeatures.csv')\n",
    "print(df)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Get operating current from PM/CALS\n",
    "- for a given magnet name get the sector number\n",
    "- query from CALS the operating current at the moment of an event\n",
    "- get beam mode at the moment of an event"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arcName = rb.getArcNameFromMagnetName(magnet)\n",
    "arcMetadata = rb.getMetadata([arcName])\n",
    "arcCurrentName = arcMetadata[arcName]['signalFGC_ABS']\n",
    "\n",
    "eventDurationInSeconds = 10e9\n",
    "t1 = u.unixTimeInNanoSecond2datatime(timestamp - eventDurationInSeconds)\n",
    "t2 = u.unixTimeInNanoSecond2datatime(timestamp + eventDurationInSeconds)\n",
    "print(t1)\n",
    "print(t2)\n",
    "print(arcCurrentName)\n",
    "signal = Signal()\n",
    "signal.read('cals', True, name=arcCurrentName, startTime=t1, endTime=t2)\n",
    "arcCurrent = signal.getSynchronizedDF()\n",
    "arcCurrent.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# For a regular QH discharge calculate\n",
    "- time constant for voltage (from exponential signal squared and logarithm)\n",
    "- time constant for current (MB)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider an exponential decay with a known initial value $f_0$ and time constant $\\tau$\n",
    "\\begin{equation}\n",
    "f(t) = f_0 e^{-t/\\tau}.\n",
    "\\end{equation}\n",
    "\n",
    "The square of the exponential function $f(t)$ reads\n",
    "\\begin{equation}\n",
    "f^2(t) = f_0^2 e^{-2t/\\tau}.\n",
    "\\end{equation}\n",
    "\n",
    "The time integral of $f^2(t)$ for $t \\in [t_0, t_1]$ is given as\n",
    "\\begin{equation}\n",
    "    \\int_{\\theta_0}^{\\theta_1} f^2(t) d\\theta = \n",
    "    \\int_{\\theta_0}^{\\theta_1} f_0^2 e^{-2t/\\tau} d\\theta = \n",
    "    -\\frac{\\tau}{2}f_0^2e^{-2t/\\tau}\\Big|_{\\theta_0}^{\\theta_1} =\n",
    "    -\\frac{\\tau}{2}f_0^2e^{-2\\theta_1/\\tau} - \\left(-\\frac{\\tau}{2}f_0^2e^{-2\\theta_0/\\tau}\\right)\n",
    "\\end{equation}\n",
    "\n",
    "We find that $\\frac{\\tau}{2}f_0^2e^{-2t_1/\\tau} = f^2(t_1)$ and analogously $\\frac{\\tau}{2}f_0^2e^{-2t_0/\\tau} = f^2(t_0)$.As a result, the square of an exponent can be written as\n",
    "\\begin{equation}\n",
    "    \\int_{\\theta_0}^{\\theta_1} f^2(t) d\\theta = \\frac{\\tau}{2}(f^2(t_0)-f^2(t_1)).\n",
    "\\end{equation}\n",
    "\n",
    "To conclude, knowing the profile of an exponential decay $f(t)$ for $t \\in [t_0, t_1]$ and assuming that the time constant does not change, i.e., $\\partial_t \\tau = 0$, the time constant is calculated as\n",
    "\\begin{equation}\n",
    "    \\tau = 2 \\frac{\\int_{\\theta_0}^{\\theta_1} f^2(t) d\\theta}{f^2(t_0)-f^2(t_1)}.\n",
    "\\end{equation}\n",
    "\n",
    "Thus, from an exponential decay of the voltage in a QH capacitor bank, i.e., $u(t) = U_0 e^{-t/\\tau}$, where $\\tau = RC$ with resistance $R$ and capacitance $C$, the time constant can be immediately calculated.\n",
    "\n",
    "\n",
    "Following the same reasoning, it can be also applied to calculate the time constant from the RC current discharge\n",
    "\\begin{equation}\n",
    "    i(t) = C\\partial_t{u(t)} = -\\frac{CU_0}{\\tau}e^{-t/\\tau} = \n",
    "    -\\frac{CU_0}{RC}e^{-t/\\tau} = - \\frac{U_0}{R}e^{-t/\\tau} = \n",
    "    -I_0 e^{-t/\\tau}.\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time constant for voltage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "signalHDS = \"U_HDS_1\"\n",
    "myDF = QhDischarge.getSynchronizedDF()[signalHDS]\n",
    "\n",
    "decayTimeConstant = pmu.getDecayTimeConstant(myDF, timeUnitMultiplier=1000)\n",
    "\n",
    "print(\"Decay time constant = %.1f ms\" % decayTimeConstant)\n",
    "\n",
    "myDFSliced = pmu.getWindowWithGivenMeanAndStd(myDF)  \n",
    "myDF.plot(xlim=(0.005,0.0165), ylim=(65,100), figsize=(20,5))\n",
    "myDFSliced.plot(xlim=(0.005,0.0165), ylim=(65,100), figsize=(20,5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time constant for current"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signalHDS = \"I_HDS_1\"\n",
    "myDF = QhDischarge.getSynchronizedDF()[signalHDS]\n",
    "\n",
    "decayTimeConstant = pmu.getDecayTimeConstant(myDF, timeUnitMultiplier=1000)\n",
    "\n",
    "print(\"Decay time constant = %.1f ms\" % decayTimeConstant)\n",
    "\n",
    "myDFSliced = pmu.getWindowWithGivenMeanAndStd(myDF)  \n",
    "myDF.plot(xlim=(0.005,0.0165), ylim=(65,100), figsize=(20,5))\n",
    "myDFSliced.plot(xlim=(0.005,0.0165), ylim=(65,100), figsize=(20,5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# For a regular QH discharge calculate\n",
    "- calculate resistance (MB)\n",
    "- calculate derivative of resistance (MB)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rNames = metadata['rNames']\n",
    "dRNames = metadata['dRNames']\n",
    "Signal.calculateResistances(QhDischarge.df, uNames, iNames, rNames)\n",
    "Signal.filterResistancesForCurrentBelowThreshold(QhDischarge.df, iNames, rNames)\n",
    "Signal.calculateTimeDerivativeOfSignals(QhDischarge.df, QhDischarge.getSynchronizedTime(), rNames, dRNames)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "myDF = QhDischarge.getSynchronizedDF()\n",
    "myDF.filter(regex=(\"U_HDS_\\d{1}\")).plot(title='Voltages', x=None, logy=False, figsize=(15,5), grid=True, fontsize=\"xx-large\")\n",
    "myDF.filter(regex=(\"U_HDS_\\d{1}\")).plot(title='Voltages (in Log)', x=None, logy=True, figsize=(15,5), grid=True, fontsize=\"xx-large\")\n",
    "myDF.filter(regex=(\"U_HDS_\\d{1}\")).plot(title='Voltages (zoom to initial part)', x=None, logy=False, figsize=(5, 5), xlim=(0.005,0.008), ylim=(850,950), grid=True, fontsize=\"xx-large\")\n",
    "myDF.filter(regex=(\"I_HDS_\\d{1}\")).plot(title='Currents', x=None, logy=False, figsize=(15,5), grid=True, fontsize=\"xx-large\")\n",
    "myDF.filter(regex=(\"I_HDS_\\d{1}\")).plot(title='Currents (in log)', x=None, logy=True, figsize=(15,5), grid=True,fontsize=\"xx-large\")\n",
    "myDF.filter(regex=(\"I_HDS_\\d{1}\")).plot(title='Currents (zoom to initial part)', x=None, logy=False, figsize=(5,5), xlim=(0.006,0.007), ylim=(80,100), grid=True, fontsize=\"xx-large\")\n",
    "myDF.filter(regex=(\"^R_HDS_\\d{1}\")).plot(title='Resistances', x=None, logy=False, ylim=(10,14), figsize=(15,5), grid=True, fontsize=\"xx-large\")\n",
    "myDF.filter(regex=(\"dR_HDS_\\d{1}\")).plot(title='dResistances', x=None, logy=False, figsize=(15,5), ylim=(-1000,1000), grid=True)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
