import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from utilities.influx.signalInfluxDb import Signal

print("Create pandas DataFrame")

Fs = 8000
f = 5
sample = 1000
x = np.arange(sample)
y = np.sin(2 * np.pi * f * x / Fs)

date_today = datetime.now()
days = pd.date_range(date_today, date_today + timedelta(len(x)-1), freq='D')

df = pd.DataFrame({'test': days, 'col2': y})
df = df.set_index('test')
print(df)

print("Create signal instance")
sw_signal = Signal(df_=df, name_='sine_wave')
print(sw_signal.df)



sw_signal.write()
sw_signal_queried = Signal(name_='sine_wave')
sw_signal_queried.read()
print(sw_signal_queried.df)


