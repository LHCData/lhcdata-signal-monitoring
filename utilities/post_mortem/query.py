import os
import sys
from pathlib import Path

curr_dir = Path(os.path.split(os.getcwd())[0])
utilities_dir = str(curr_dir.parent / 'utilities')

if utilities_dir not in sys.path:
	sys.path.append(utilities_dir)

import pandas as pd
import utilities.utils as u
import requests


class Query(object):

	def __init__(self, system={}, className={}, source={}, signal={}, eventTimeStamp={}):
		self.System = system
		self.ClassName = className
		self.Source = source
		self.Signal = signal
		self.EventTimeStamp = eventTimeStamp

	def generateQuery(self):
		""" Function returns hyperlink ready to query """
		self.EventTimeStamp = pd.Timestamp(self.EventTimeStamp)
		self.EventTimeStamp = u.timestamp2unixtimeInNanoSecond(self.EventTimeStamp)
		query = "http://pm-api-pro/v2/pmdata/signal?system=" + self.System + "&className=" + self.ClassName + "&source=" + self.Source + "&timestampInNanos=" + str(
			self.EventTimeStamp) + "&signal=" + self.Signal
		return query

	def executeQuery(self, query):
		return requests.get(query)
