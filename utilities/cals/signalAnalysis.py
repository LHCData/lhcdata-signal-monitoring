import os
import sys
from pathlib import Path

curr_dir = Path(os.path.split(os.getcwd())[0])
utilities_dir = str(curr_dir.parent / 'utilities')

if utilities_dir not in sys.path:
	sys.path.append(utilities_dir)

import numpy as np
import pandas as pd
# self made functions
import utilities.cals.signalCALS as scals
import utilities.editDF as edf

# scipy imports
from scipy import stats
from scipy.stats import norm


class SignalAnalysis():

	def getSignalParts(bMode={}, triggerLow={}, Location={}, triggerHigh={}):
		""" Function returns the full signal and the signal before and after the ramps """
		if triggerHigh == {}:
			(start, triggerLow, triggerHigh, end) = edf.EditDF.timeFromBmode(bMode, triggerLow)
		else:
			start = pd.Timestamp(triggerLow) + pd.DateOffset(minutes=-30)
			end = pd.Timestamp(triggerHigh) + pd.DateOffset(minutes=30)

		vDF = scals.Signal(Location, start, end).getSignal(Location)
		lowVoltage = scals.Signal(Location, start, triggerLow).getSignal(Location)
		highVoltage = scals.Signal(Location, triggerHigh, end).getSignal(Location)
		return (vDF, lowVoltage, highVoltage)

	def linReg(Signal, voltageName):
		""" Function returns linreg function and derivertive of given Dataset"""
		v = Signal.loc[voltageName].values
		x = edf.EditDF.time(Signal, 'years')
		res = stats.mstats.theilslopes(v, x, 0.95)
		x = [res[1] + res[0] * value for value in x]
		return x, res[0]

	def snr(array):
		""" calculates the signal to noise ratio"""
		(mu, sigma) = norm.fit(array)
		snr = abs(mu) / abs(sigma)
		return snr

	def rejectOutliers(data, m=4):
		""" rejects data which is outside m *sigma """
		return data[abs(data - np.mean(data)) < m * np.std(data)]

	def percentage(a, b):
		""" returns ratio in percentage """
		return 100 * float(a) / float(b)
