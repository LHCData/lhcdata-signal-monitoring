import os
import sys
from pathlib import Path

curr_dir = Path(os.path.split(os.getcwd())[0])
utilities_dir = str(curr_dir.parent / 'utilities')

if utilities_dir not in sys.path:
	sys.path.append(utilities_dir)

# Dataframe filtering with widgets
import pandas as pd
# self made functions
# https://github.com/rdemaria/pytimber
import pytimber

ldb = pytimber.LoggingDB()

import ipywidgets as widgets


class FilterToolbox():

	def importFilterWidgets(resistance):
		""" Function returns preset widgets """
		filt = widgets.SelectMultiple(
			options=['Location: Sector', 'Location: R bigger than'],
			value=['Location: R bigger than'],
			description='F0ilter',
			disabled=False)

		startTime = widgets.Dropdown(
			options=list(resistance.columns.values),
			value=resistance.columns.values[0],
			description='Start Time:',
			disabled=False)

		endTime = widgets.Dropdown(
			options=list(resistance.columns.values),
			value=resistance.columns.values[-1],
			description='End Time:',
			disabled=False)

		sector = widgets.Dropdown(
			options=['all', '12', '23', '34', '45', '56', '67', '78', '81'],
			value='all',
			description='Sector:',
			disabled=False)

		feature = widgets.Dropdown(
			options=[r"R [nOhm]", r"dR/dt [nOhm/year]", r"SNR"],
			value=r"R [nOhm]",
			description='Feature:',
			disabled=False, )

		featureMargin = widgets.FloatText(
			value=11.5,
			description=r"Margin Value:", )
		return filt, startTime, endTime, sector, feature, featureMargin

	def filterAll(resistance, locationParameter, startTime, endTime, sector, feature, featureMargin):
		""" Function filters Dataframe with widgets """
		res = resistance
		# TIME
		res = FilterToolbox.timeRange(res, startTime, endTime)
		# LOCATION
		if sector != 'all':
			res = FilterToolbox.sector(res, sector)
		# FEATURE
		if feature == r"R [nOhm]":
			res = FilterToolbox.biggerThan(res, res[res.columns.values[0]],
			                               float(featureMargin) * 1e-9)  # Dataframe, guide Data, margin
		elif feature == r"SNR":
			res = FilterToolbox.biggerThan(res, locationParameter[locationParameter.columns.values[0]],
			                               float(featureMargin))
		elif feature == r"dR/dt [nOhm/year]":
			res = FilterToolbox.biggerThan(res, locationParameter[locationParameter.columns.values[1]],
			                               float(featureMargin) * 1e-9)

		filterT = FilterToolbox.filterText(resistance, res, startTime, endTime, sector, feature, featureMargin)

		return res, filterT

	def filterText(resistance, res, startTime, endTime, sector, feature, featureMargin):
		filterT = 'Filter settings:' \
		          + '\n' + "Chosen time period: " \
		          + '\n' + str(startTime) + "  to  " + str(endTime) \
		          + '\n' + "Chosen sector: " + str(sector) \
		          + '\n' + "Chosen feature:" \
		          + '\n' + "All values of " + str(feature) + " bigger than: " + str(featureMargin) \
		          + '\n' + " -> " + str(
			100 * (len(res.index.values) / len(resistance.index.values))) + '% of all Signals have been taken ' + '\n'

		return filterT

	def timeRange(resistance, startTime, endTime):
		trange = resistance.loc[:, startTime:endTime]
		return trange

	def sector(resistance, s):
		""" Function returns chosen sector as DF """
		likeL = s[0] + '.R'
		likeR = s[1] + '.L'
		sectorL = resistance.filter(like=likeL, axis=0)
		sectorR = resistance.filter(like=likeR, axis=0)
		sector = pd.concat([sectorL, sectorR])
		return sector

	def biggerThan(resistance, rOne, margin):
		""" Function returns Signals bigger than margin"""
		# rOne=locationParameter[locationParameter.columns.values[0]] #first row of data
		rOneBig = rOne[rOne.values > margin]
		indexOfBigSignals = rOneBig.index.values
		bigSignals = resistance[resistance.index.isin(indexOfBigSignals)]
		return bigSignals

	def smallerThan(resistance, margin):
		""" Function returns Signals smaller than margin"""
		rOne = resistance[resistance.columns.values[0]]
		rOneBig = rOne[rOne.values < margin]
		indexOfBigSignals = rOneBig.index.values
		bigSignals = resistance[resistance.index.isin(indexOfBigSignals)]
		return bigSignals

	def matchCurrent(voltageName):
		""" Function finds current at certain location """
		currentName = 'RPHE%' + voltageName[3] + '.A'
		if voltageName[-7] == 'R':
			currentName = currentName + voltageName[-9] + '%:I_MEAS'
		elif voltageName[-7] == 'L':
			if int(voltageName[-9]) == 8:
				currentName = currentName + str(1) + '%:I_MEAS'
			else:
				currentName = currentName + str(int(voltageName[-9]) + 1) + '%:I_MEAS'
		currentName = ldb.search(currentName)
		if currentName == []:
			print('could not generate current name')
		else:
			return currentName[0]
