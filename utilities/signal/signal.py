from utilities.influx import signalInfluxDb
from utilities.signal import df_signal
from utilities.cals.signalCALS import Signal as SignalCALS
from utilities.post_mortem.signalPM import Signal as SignalPM
from utilities.influx.signalInfluxDb import Signal as SignalInflux
from utilities.signal.dataFrameIndexConverter import DataFrameIndexConverter
import pandas as pd
import scipy.signal as spsig
from abc import abstractmethod, ABCMeta
from copy import deepcopy
import numpy as np


class Signal(object):
    """Signal class encapsulates behavior for working with signals. It provide read and write methods for accessing
    Post Mortem, CALS and InfluxDB APIs. Signal is a client of of the DFSignal classes"""

    def __init__(self, name=None, df=None):
        # region set States
        self.nsState = IndexInNanoSecondsState(self)
        self.sState = IndexInSecondsState(self)
        self.tsState = IndexInTimestampsState(self)
        self.indexState = self.nsState
        # endregion
        self.name = name
        self.__df = pd.DataFrame()
        self._dbAccess = None
        if df is not None:
            self.df = df

    #region df property

    @property
    def df(self):
        return self.__df

    @df.setter
    def df(self, df):
        """Method validates that the passed DataFrame are indexed with unix time in nanoseconds"""
        if df is not None:
            if not isinstance(df, pd.DataFrame):
                raise TypeError(
                    "You can only assign a pandas DataFrame to signal.df. The DataFrame needs to be formatted with a index of unix time in nanoseconds")

            index = df.index.tolist()
            if not isinstance(index[0], int):
                raise TypeError("The index of the passed DataFrame needs to be unix time in nanoseconds (int64).")

            self.indexState = self.nsState

        self.__df = df

    #endregion

    #region dbAccess property

    @property
    def dbAccess(self):
        return self._dbAccess

    @dbAccess.setter
    def dbAccess(self, dbAccess):
        """Method sets the dbAccess attribute1  of the signal"""
        if dbAccess is not None:
            if not isinstance(dbAccess, df_signal.DFSignal):
                raise TypeError(
                    "You passed a object of type {0}, provide one of the following: signalPM.Signal, signalCALS.Signal or signalInflux.Signal".format(
                        type(dbAccess)))

        self._dbAccess = dbAccess

    # def setDBAccess(self, dbAccess):
    #     """This method sets the dbAccess attribute1  of the signal"""
    #     if isinstance(dbAccess, df_signal.DFSignal):
    #         self._dbAccess = dbAccess
    #     else:
    #         raise TypeError(
    #             "You passed {}, provide one of the following: Signal, Signal or Signal".format(
    #                 type(dbAccess)))

    #endregion

    def write(self, api='',  **kwargs):
        """Method uses the dbAccess attribute to write the signal to the database, if the api argument is passed then then the
        apiHandler method is called to set a behavior, otherwise current is used.
        The signal instance and the provided kwargs passed on to the behavior write call"""
        if api:
            self.dbAccess = self.createDFSignal(api, **kwargs)

        if self.dbAccess:
            return self._databaseAccessHandler(self.dbAccess.write, **kwargs)

    def read(self, api='', append=False, **kwargs):
        if api:
            self.dbAccess = self.createDFSignal(api, **kwargs)

        if self.dbAccess:
            df = self._databaseAccessHandler(self.dbAccess.read, **kwargs)
            if append:
                self.df = pd.concat([df, self.df], axis=1)
            else:
                self.df = df

    def _databaseAccessHandler(self, access_method, **kwargs):
        """Method provide a framework accessing a database used by the read and write methods. if an api parameter is
        provided the dbAccess property to a new object of one of the derived DFSignal types.
        Then the provided access method (read or write) is performed, an exception is thrown if dbAccess is None.
        The current index state is maintained"""

        startingState = self.indexState

        if self.dbAccess is None:
            raise TypeError(
                "No API selected, either provide an api argument at least once or provide one using the dbAccess property")

        accessResult = access_method(self, **kwargs)
        self.indexState.convertToState(startingState)
        return accessResult


    @staticmethod
    def createDFSignal(signalType, **kwargs):
        """Method returns an object of one of the derived DFSignal classes according to the passed signalType"""
        dfSignal = None

        signalType_ = signalType.upper()
        if signalType_ == "PM":
            dfSignal = SignalPM.createSignalPM(**kwargs)
        elif signalType_ == "CALS":
            dfSignal = SignalCALS.createSignalCALS(**kwargs)
        elif signalType_ == "INFLUX":
            dfSignal = SignalInflux.createSignalInflux(**kwargs)

        return dfSignal


    # region index conversion methods

    def convertIndexToNanoseconds(self):
        """Method uses the index state machine to convert the index of the signal's DataFrame to nanoseconds"""
        if not self.df.equals(pd.DataFrame()):
            self.indexState.convertToState(self.nsState)

    def convertIndexToSeconds(self):
        """Method uses the index state machine to convert the index of the signal's DataFrame to seconds"""
        if not self.df.empty and self.df is not None:
            self.indexState.convertToState(self.sState)

    def convertIndexToTimestamps(self):
        """Method uses the index state machine to convert the index of the signal's DataFrame to timestamps"""
        if not self.df.empty and self.df is not None:
            self.indexState.convertToState(self.tsState)

    def getSynchronizedDF(self):
        """Method returns a DataFrame object where the index is synchronized seconds"""
        if not self.df.empty and self.df is not None:
            startingState = self.indexState
            self.convertIndexToSeconds()
            df_ = deepcopy(self.df)
            self.indexState.convertToState(startingState)
            DataFrameIndexConverter.convertDataFrameIndex(df_, lambda s: s - df_.index[0])

            return df_
        
    def getSynchronizedTime(self):
        """Method returns a DataFrame object where the index is synchronized seconds"""
        if not self.df.empty and self.df is not None:
            df_ = deepcopy(self.df)
            time = df_.index.tolist()
            time = [t/1e9-time[0]/1e9 for t in time]

            return time

    # endregion
    
# region static methods
    @staticmethod
    def getPmSignals(frameName, signalNames, system, className, source, timestampInNanos, magnetType=''):
        """Method creates and returns a Signal object which contains all the signals described in signalNames parameters"""
        pmSignals = Signal(frameName)

        #query measurements:
        for signalName in signalNames:
            pmSignals.read('pm',append=True, system=system, className=className, source=source, signalName=signalName, eventTime=timestampInNanos)

        return pmSignals

    @staticmethod
    def getFeatureDataFrame(dataFrame):
        """Method returns a dataFrame with certain features based on the passed DataFrame object"""
        return pd.DataFrame([dataFrame.max(), dataFrame.min(), dataFrame.mean(), dataFrame.std(), dataFrame.iloc[0], dataFrame.iloc[-1]],['max', 'min', 'avg', 'std', 'start', 'end'])
    
    def getFeatureRowDataFrame(dataFrame, timestamp):
        
        columns = ['{}_max'.format(a) for a in dataFrame.columns]\
                + ['{}_min'.format(a) for a in dataFrame.columns]\
                + ['{}_avg'.format(a) for a in dataFrame.columns]\
                + ['{}_std'.format(a) for a in dataFrame.columns]\
                + ['{}_0'.format(a) for a in dataFrame.columns]\
                + ['{}_end'.format(a) for a in dataFrame.columns]

        df = pd.DataFrame(columns=columns)
        values = np.append(dataFrame.max().values, dataFrame.max().values)
        values = np.append(values, dataFrame.mean().values)
        values = np.append(values, dataFrame.std().values)
        values = np.append(values, dataFrame.iloc[0])
        values = np.append(values, dataFrame.iloc[-1])

        df.loc[0] = values
        df.index = [timestamp]
        df.index.name = 'timestamp'
        
        return df
    
    @staticmethod
    def filterSignalNames(dataFrame, filterNames, kernelSize=3):
        """Method filters all coulmns in the passed DataFrame correlating with the names described in filterNames. It does this by applying scipy.signal's medfilt method to these columns"""
        for nameToFilter in filterNames:
            dataFrame[nameToFilter] = spsig.medfilt(dataFrame[nameToFilter], kernelSize)
            
    @staticmethod
    def calculateResistances(dataFrame, uNames, iNames, rNames):
        """Method calculates and appends resistance values to passed DataFrame based on the passed uNames and iNames"""
        for uName, iName, rName in zip(uNames, iNames, rNames):
            dataFrame[rName] = dataFrame[uName] / dataFrame[iName]
        
    @staticmethod
    def filterResistancesForCurrentBelowThreshold(dataFrame, iNames, rNames, currentThresholdInAmps=1):
        """Method filters alle resistance values for current values below a certain value"""
        for iName, rName in zip(iNames, rNames):
            select_indices = list(np.where(abs(dataFrame[iName]) <= currentThresholdInAmps)[0])
            dataFrame[rName].iloc[select_indices] = 0  
    
    @staticmethod
    def calculateTimeDerivativeOfSignals(dataFrame, timeFrame, sourceSignalNames, derivativeSignalNames, kernelSize=51):
        """Method calculate time derivatives for resistance values contained in the passed DataFrame object"""
        for sourceName, derivativeName in zip(sourceSignalNames, derivativeSignalNames):
            Signal.calculateTimeDerivativeOfSignal(dataFrame, timeFrame, sourceName, derivativeName, kernelSize)


    @staticmethod
    def calculateTimeDerivativeOfSignal(dataFrame, timeFrame, sourceSignal, derivativeName, kernelSize=51):
        """Method calculate time derivative for a single Signal"""
        dataFrame[derivativeName] = spsig.medfilt(np.gradient(dataFrame[sourceSignal]) / np.gradient(timeFrame), kernelSize)

#endregion



#endregion
    

#region index states

class DataFrameIndexState(metaclass=ABCMeta):
    """Base class for the index state of the signal's DataFrame"""

    @abstractmethod
    def convertIndexToNanoseconds(self):
        """base implementation for converting index to ns from current state"""
        pass

    @abstractmethod
    def convertIndexToThisStateFromNanoseconds(self):
        """base implementation for converting index to current state from ns"""
        pass

    def convertToState(self, endState):
        """Method validates that endState is different from the current indexState, then first converts the index to ns and
        then to the specified index type. Lastly the indexState of the signal is reassigned"""
        if endState is not self:
            self.convertIndexToNanoseconds()
            endState.convertIndexToThisStateFromNanoseconds()
            self.signal.indexState = endState


class IndexInNanoSecondsState(DataFrameIndexState):
    """class for defining the index state of nanoseconds"""

    def __init__(self, signal):
        self.signal = signal

    def convertIndexToNanoseconds(self):
        pass

    def convertIndexToThisStateFromNanoseconds(self):
        pass


class IndexInSecondsState(DataFrameIndexState):
    """class for defining the index state of seconds"""

    def __init__(self, signal):
        self.signal = signal

    def convertIndexToNanoseconds(self):
        """Method converts the signal's DataFrame to nanoseconds from seconds"""
        DataFrameIndexConverter.convertIndexFromSecondsToNanoseconds(self.signal.df)

    def convertIndexToThisStateFromNanoseconds(self):
        """Method converts the signal's DataFrame to seconds from nanoseconds"""
        DataFrameIndexConverter.convertIndexFromNanosecondsToSeconds(self.signal.df)


class IndexInTimestampsState(DataFrameIndexState):

    def __init__(self, signal):
        self.signal = signal

    def convertIndexToNanoseconds(self):
        """Method converts the signal's DataFrame to nanoseconds from pandas Timestamps"""
        DataFrameIndexConverter.convertIndexFromTimestampsToNanoseconds(self.signal.df)

    def convertIndexToThisStateFromNanoseconds(self):
        """Method converts the signal's DataFrame to pandas Timestamps from nanoseconds"""
        DataFrameIndexConverter.convertIndexFromNanosecondsToTimestamps(self.signal.df)

#endregion