import os
import sys
from pathlib import Path

curr_dir = Path(os.path.split(os.getcwd())[0])
utilities_dir = str(curr_dir.parent / 'utilities')

if utilities_dir not in sys.path:
	sys.path.append(utilities_dir)

import pandas as pd
import numpy as np
import utils as u
import pytimber

ldb = pytimber.LoggingDB()


class Resistance(object):

	def __init__(self, voltage={}, current={}, triggerLow={}, triggerHigh={}, startTime={}, endTime={}):
		self.Voltage = voltage
		self.Current = current
		self.TriggerLow = triggerLow
		self.TriggerHigh = triggerHigh
		self.StartTime = startTime
		self.EndTime = endTime

	def getResistanceDF(self):
		""" Function is linking voltages and currents & calculates the resistances """
		TriggerLowStamp = self.TriggerLow
		TriggerHighStamp = self.TriggerHigh
		self.TriggerLow = u.unixTimeInNanoSecond2datatime(float(TriggerLowStamp) * 1e9)
		self.TriggerHigh = u.unixTimeInNanoSecond2datatime(float(TriggerHighStamp) * 1e9)

		# start and endTime will be adjusted to +/- 30min of flag
		self.StartTime = u.unixTimeInNanoSecond2datatime((float(TriggerLowStamp) - 3600 / 2) * 1e9)
		self.EndTime = u.unixTimeInNanoSecond2datatime((float(TriggerHighStamp) + 3600 / 2) * 1e9)

		vAvg = self.getVoltage()
		iAvg = self.getCurrent()

		resistance = [0.0 for x in range(0, len(self.Voltage))]
		# for each voltage find a current
		for r in range(0, len(self.Voltage)):
			for s in range(0, len(self.Current)):
				posI = self.Current[s].find(':I_MEAS')
				sector = self.Current[s][(posI - 2):(posI)]
				iDF = self.Current[s][posI - 5]
				uDF = self.Voltage[r][3]

				# Current A78 to Voltage 7.R and 8.L
				if ((sector[0] + '.R') in self.Voltage[r]) or ((sector[1] + '.L') in self.Voltage[r]):
					if self.Voltage[r][2] == 'Q':  # if quadrupole
						if iDF == uDF:  # Focus & Defocus must be the same
							resistance[r] = vAvg[r] / iAvg[s]
					elif self.Voltage[r][2] == 'B':  # if dipol
						resistance[r] = vAvg[r] / iAvg[s]

		return pd.DataFrame(resistance, self.Voltage, [str(self.TriggerLow)])

	def getVoltage(self):
		""" Function calculating the avg voltages in given period """
		vLow = ldb.getScaled(self.Voltage, self.StartTime, self.TriggerLow, scaleAlgorithm='AVG',
		                     scaleInterval='MINUTE')
		vHigh = ldb.getScaled(self.Voltage, self.TriggerHigh, self.EndTime, scaleAlgorithm='AVG',
		                      scaleInterval='MINUTE')
		vAvg = [0.0 for x in range(0, len(self.Voltage))]  # define lenght
		for v in range(0, len(self.Voltage)):
			vAvg[v] = abs(np.nanmean(vHigh[self.Voltage[v]][1]) - np.nanmean(vLow[self.Voltage[v]][1]))
		return vAvg

	def getCurrent(self):
		""" Function calculating the avg currents in given period """
		iLow = ldb.getScaled(self.Current, self.StartTime, self.TriggerLow, scaleAlgorithm='AVG',
		                     scaleInterval='MINUTE')
		iHigh = ldb.getScaled(self.Current, self.TriggerHigh, self.EndTime, scaleAlgorithm='AVG',
		                      scaleInterval='MINUTE')
		iAvg = [0.0 for x in range(0, len(self.Current))]  # define lenght
		for i in range(0, len(self.Current)):
			iAvg[i] = np.nanmean(iHigh[self.Current[i]][1]) - np.nanmean(iLow[self.Current[i]][1])
		return iAvg
