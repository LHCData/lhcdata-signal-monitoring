
import unittest
from utilities.post_mortem.signalPM import Signal as SignalPM
import pandas as pd
from datetime import datetime

class TestSignalPM(unittest.TestCase):

    def setUp(self):
        self.defaultSignalPM = SignalPM()

    def tearDown(self):
        pass

    def assertEqualSignals(self, firstSignalPM, secondSignalPM):
        """Assert each property of firstSignalPM against of the same property of secondSignalPM"""
        self.assertEqual(firstSignalPM.System, secondSignalPM.System)
        self.assertEqual(firstSignalPM.ClassName, secondSignalPM.ClassName)
        self.assertEqual(firstSignalPM.Source, secondSignalPM.Source)
        self.assertEqual(firstSignalPM.SignalName, secondSignalPM.SignalName)
        self.assertEqual(firstSignalPM.EventTime, secondSignalPM.EventTime)
        self.assertEqual(firstSignalPM.StartTime, secondSignalPM.StartTime)
        self.assertEqual(firstSignalPM.IntervTime, secondSignalPM.StartTime)
        self.assertEqual(firstSignalPM.Unit, secondSignalPM.Unit)
        self.assertEqual(firstSignalPM.Value, secondSignalPM.Value)
        self.assertEqual(firstSignalPM.Time, secondSignalPM.Time)

    def test_createSignalPM_noParameters_defaultSignalCreated(self):
        """Tests that the default Signal is created if no arguments are passed to the createSignalPM method"""

        # act:
        pmSignal = SignalPM.createSignalPM()

        # assert:
        self.assertEqualSignals(pmSignal, self.defaultSignalPM)

    def test_createSignalPM_initParametersProvided_CorrectSignalCreated(self):
        """Tests that the correct Signal is created if arbitrary arguments are passed to the createSignalPM method"""

        # arrange:
        now = datetime.now()
        arbitraryPMSignal = SignalPM("System", "ClassName", "source", "test", pd.Timestamp(now))

        # act:
        pmSignal = SignalPM.createSignalPM(system="System",
                                           className="ClassName",
                                           source="source",
                                           signalName="test",
                                           eventTime=pd.Timestamp(now))

        # assert:
        self.assertEqualSignals(pmSignal, arbitraryPMSignal)

    def test_createSignalPM_parameterIsInvalidType_TypeErrorIsThrown(self):
        """Tests that when kwargs contain a username keyword that isn't a string, a TypeError
        is thrown"""

        now = datetime.now()

        with self.assertRaises(TypeError):
            SignalPM.createSignalPM(system=1000,
                                  className="ClassName",
                                  source="source",
                                  signalName="test",
                                  eventTime=pd.Timestamp(now))


    # needs read and getTimeValue tests

    def test_getUnit_contentContainUnit_rightUnitReturned(self):
        """Test that when content contain a unit value this unit is returned"""

        # arrange:
        content = {'valueDescriptor':{'attributes':{'test':{'units':{'value':'unit'}}}}}
        uut = SignalPM(signalName='test')

        # act:
        unit = uut.getUnit(content)

        # assert:
        self.assertEqual(unit, 'unit')

    def test_getUnit_contentDoesNotContainUnit_rightUnitReturned(self):
        """Test that when content doesn't contain a unit value 'N/A' is returned"""
        # arrange:
        content = {'valueDescriptor':{'attributes':{'test':{'units':{}}}}}
        uut = SignalPM(signalName='test')

        # act:
        unit = uut.getUnit(content)

        # assert:
        self.assertEqual(unit, 'N/A')


    def test_getNamesOfValues_correctDBAccessWriteCalled(self):
        """Test that write correctly call the write method of the correct DFSignal type dependent on the api parameter"""

        # arrange:
        uut = SignalPM()

        testCases = [("zero", {'namesAndValues': []}, []),
                     ('one', {'namesAndValues': [{'name': 'name1'}]}, ['name1']),
                     ('many', {'namesAndValues': [{'name': 'name1'}, {'name': 'name2'}]}, ['name1', 'name2'])]

        for testName, content, valueNames in testCases:
            with self.subTest(name=testName):

                # act:
                result = uut.getNamesOfValue(content)

                # assert:
                self.assertEqual(result, valueNames)

    def test_getTime_TimeIsReturned(self):
        """Test that getTime returns right Time"""


        # arrange:
        testCases = [("zero", []),
                     ('one', [1]),
                     ('many', [1,2,3])]

        for testName, time in testCases:
            with self.subTest(name=testName):
                pmSignal = SignalPM(time=time)


                # act:
                result = pmSignal.getTime()

                # assert:
                self.assertEqual(result, time)

    def test_getValue_ValueIsReturned(self):
        """Test that getTime returns right Time"""

        # arrange:
        testCases = [("zero", []),
                     ('one', [1]),
                     ('many', [1, 2, 3])]

        for testName, time in testCases:
            with self.subTest(name=testName):
                pmSignal = SignalPM(value=time)

                # act:
                result = pmSignal.getValue()

                # assert:
                self.assertEqual(result, time)