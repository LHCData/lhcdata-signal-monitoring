import os
import sys
from pathlib import Path

curr_dir = Path(os.path.split(os.getcwd())[0])
utilities_dir = str(curr_dir.parent / 'utilities')

if utilities_dir not in sys.path:
	sys.path.append(utilities_dir)

import pandas as pd
import calcResistance as r
import cals.detectBmodePattern as d
from tictoc import tic
from tictoc import toc
import pytimber

ldb = pytimber.LoggingDB()


def ResToCSV(t1, t2, vname, cname):
	""" Function calculates resistances for all ramps and saves it into .csv """
	# prepare new Folder for the csv to be stored
	resType = vname[-3]
	if vname[-3] == 'R':
		resType = 'BB'
	fname = 'R' + vname[2] + resType + '_' + t1[2:4] + t1[5:7] + t1[8:10] + '_' + t2[2:4] + t2[5:7] + t2[8:10]
	currentDirectory = os.getcwd()
	finalDirectory = os.path.join(currentDirectory, r'Database')
	if not os.path.exists(finalDirectory):
		os.makedirs(finalDirectory)

	# Look for signal names
	voltage = ldb.search(vname)
	current = ldb.search(cname)
	print(fname + ': Number of signals: ' + str([len(voltage), len(current)]))

	# Look for ramps
	bMode = 'HX:BMODE'
	[low, high] = d.DetectBmodePattern(t1, t2, bMode).Stamp()
	print(fname + ': Number of ramps: ' + str([len(low), len(high)]))

	# Look for signal values
	tic()
	if len(low) != 0:
		resistance = r.Resistance(voltage, current, low[0], high[0]).getResistanceDF()
		print(fname + ": Ramp " + str(1) + " of " + str(len(low)))
		resistance.to_csv(finalDirectory + '/' + fname + '.csv')
		toc()
		for l in range(1, len(low)):
			newFrame = r.Resistance(voltage, current, low[l], high[l]).getResistanceDF()
			resistance = pd.concat([resistance, newFrame], axis=1, join='inner')
			resistance.to_csv(finalDirectory + '/' + fname + '.csv')
			print(fname + ": Ramp " + str(l + 1) + " of " + str(len(low)))
			toc()
	else:
		resistance = pd.DataFrame([0])
		resistance.to_csv(finalDirectory + '/' + fname + '.csv')
		print('No data during ' + t1 + ' and ' + t2)
