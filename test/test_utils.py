
import unittest
import pandas as pd
from utilities import utils as u
from time import gmtime, strftime
from datetime import datetime

class TestUtils(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_util_unixTimeInNanoSecond2datatime_returnsRightString(self):
        """Tests that unixTimeInNanoSecond2datatime returns a correctly formatted string when passed a unix
        timestamp"""
        #A better test would be independant of the system time zone. But the tested method is dependant on local time.
        #This test, tests the result against the time zone of the machine it was written on (+0100) and that of the test build server (UTC).
        #This test would likely fail if run on a sytstem with a time zone diffent than those two.

        #arrange
        testCases = [
            (0,                     "1970-01-01 00:00:00.00",   "1970-01-01 01:00:00.000"),
            (1528374951891000000,   '2018-06-07 12:35:51.35',   '2018-06-07 14:35:51.891'),
            (1552306888281934000,   '2019-03-11 12:21:28.21',   '2019-03-11 13:21:28.281'),
            (1867926088300000000,   '2029-03-11 12:21:28.21',   '2029-03-11 13:21:28.300'),
            (1867926088300000001,   '2029-03-11 12:21:28.21',   '2029-03-11 13:21:28.300')
        ]

        # uses subTest to test a set of test cases:
        for unixTime, refUTC, ref0100 in testCases:
            with self.subTest(name=str(unixTime)):
                #act
                timestampResult = u.unixTimeInNanoSecond2datatime(unixTime)
                #assert
                if(strftime("%z", gmtime()) == "+0100"):
                    self.assertEqual(ref0100, timestampResult)
                else:
                    self.assertEqual(refUTC, timestampResult)

    # def test_util_unixTimeInNanoSecond2dateTime_returnsRightdatetime(self):
    #     """Tests that unixTimeInNanoSeconds2dateTime returns a correctly formatted dateTime object when passed a unix
    #     timestamp"""
    #
    #     #A better test would be independant on the system time zone. But the tested method is dependant on local time.
    #     #This test tests the result against the time zone of the machine (+0100) it was written on and that of the test build server (UTC).
    #     #This test would likely fail if run on a system with a time zone different than those two.
    #
    #     #arrange
    #     testCases = [
    #         (0,                   datetime(1970, 1, 1, 0, 0, 0, 0),          datetime(1970, 1, 1, 1, 0, 0, 0)),
    #         (1528374951891000000, datetime(2018, 6, 7, 12, 35, 51, 891000),  datetime(2018, 6, 7, 14,35,51, 891000)),
    #         (1552306888281934000, datetime(2019, 3, 11, 12, 21, 28, 281934), datetime(2019, 3, 11, 13, 21, 28, 281934)),
    #         (1867926088300000000, datetime(2029, 3, 11, 12,21, 28, 300000),  datetime(2029,3,11,13,21,28,300000)),
    #         (1867926088300000001, datetime(2029, 3, 11, 12, 21, 28, 300000), datetime(2029, 3, 11, 13,21,28,300000))
    #     ]
    #
    #     # uses subTest to test a set of test cases:
    #     for unixTime, refUTC, ref0100 in testCases:
    #         with self.subTest(name=str(unixTime)):
    #             # act
    #             timestampResult = u.unixTimeInNanoSeconds2dateTime(unixTime)
    #             # assert
    #             if strftime("%z", gmtime()) == "+0100":
    #                 self.assertEqual(ref0100, timestampResult)
    #             else:
    #                 self.assertEqual(refUTC, timestampResult)

    def test_util_datetime2unixTimeInSecond_returnsRightValue(self):
        """Tests that datetime2unixTimeInSecond returns correct unix time in seconds when passed a datetime object"""

        # arrange:
        testCases = [
            (0, datetime(1970, 1, 1, 0, 0, 0, 0)),
            (1528374951.891, datetime(2018, 6, 7, 12, 35, 51, 891000)),
            (1552306888.281934, datetime(2019, 3, 11, 12, 21, 28, 281934)),
            (1867926088.3, datetime(2029, 3, 11, 12, 21, 28, 300000)),
        ]

        # uses subTest to test a set of test cases:
        for secs, datetimeObj in testCases:
            with self.subTest(name=str(datetimeObj)):
                # act
                secsResult = u.datetime2unixtimeInSecond(datetimeObj)
                # assert
                self.assertEqual(secs, secsResult)

    def test_util_datetime2unixTimeInNanoSecond_returnsRightValue(self):
        """Tests that datetime2unixTimeInNanoSecond returns correct unix time in nanoseconds when passed a datetime object"""

        # arrange:
        testCases = [
            (0, datetime(1970, 1, 1, 0, 0, 0, 0)),
            (1.528374951891e+18, datetime(2018, 6, 7, 12, 35, 51, 891000)),
            (1.552306888281934e+18, datetime(2019, 3, 11, 12, 21, 28, 281934)),
            (1.8679260883e+18, datetime(2029, 3, 11, 12, 21, 28, 300000)),
        ]

        # uses subTest to test a set of test cases:
        for ns, datetimeObj in testCases:
            with self.subTest(name=str(datetimeObj)):
                # act
                nsResult = u.datetime2unixtimeInNanoSecond(datetimeObj)
                # assert
                self.assertEqual(ns, nsResult)

    def test_util_timestamp2unixtimeInNanoSecond_returnRightValue(self):
        """Tests that timestampunixtimeInNanoSecond returns correctly formatted pandas timestamp object when passed
        a unix time in nanoseconds"""

        # arrange
        testCases = [
            (pd.Timestamp('2018-06-07 14:35:51.891', tz='Europe/Zurich'), 1528374951891000000)
        ]

        # uses subTest to test a set of test cases:
        for timestamp, ns in testCases:
            with self.subTest(name=timestamp):
                # act
                result = u.timestamp2unixtimeInNanoSecond(timestamp)
                # assert
                self.assertEqual(result, ns)

