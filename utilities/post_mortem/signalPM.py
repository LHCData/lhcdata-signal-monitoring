import requests
import json
import pandas as pd
import utilities.post_mortem.query as q
from utilities.signal.df_signal import DFSignal


class Signal(DFSignal):

    def __init__(self, system={}, className={}, source={}, signalName={}, eventTime={}, startTime={}, intervTime={},
                 unit={}, value={}, time={}):
        super().__init__('PM')
        self.System = system
        self.ClassName = className
        self.Source = source
        self.SignalName = signalName
        self.EventTime = eventTime
        self.StartTime = startTime
        self.IntervTime = intervTime
        self.Unit = unit
        self.Value = value
        self.Time = time

    @staticmethod
    def createSignalPM(**kwargs):
        """Method Validates kwargs and returns a Signal object.
        The method looks through kwargs for arguments to create a new Signal. If arguments are not provided, default
        arguments are used"""
        system = Signal.validateKeyInkwargs('system', str, **kwargs) or {}
        className = Signal.validateKeyInkwargs('className', str, **kwargs) or {}
        source = Signal.validateKeyInkwargs('source', str, **kwargs) or {}
        signalName = Signal.validateKeyInkwargs('signalName', str, **kwargs) or {}
        eventTime = Signal.validateKeyInkwargs('eventTime', int, pd.Timestamp, **kwargs) or {}
        intervTime = Signal.validateKeyInkwargs('intervTime', int, pd.Timestamp, **kwargs) or {}
        unit = Signal.validateKeyInkwargs('unit', str, **kwargs) or {}
        time = Signal.validateKeyInkwargs('time', list, **kwargs) or {}
        value = Signal.validateKeyInkwargs('value', list, **kwargs) or {}

        return Signal(system, className, source, signalName, eventTime, intervTime, unit, time, value)

    def parseSignal(self):
        """Method looks for structure parameter and time and returns Dataframe"""
        request = q.Query(self.System, self.ClassName, self.Source, self.SignalName, self.EventTime).generateQuery()
        content = json.loads(requests.get(request).text)['content'][0]


        # find names
        nameOfValue = Signal.getNamesOfValue(self, content)

        self.Unit = Signal.getUnit(self, content)

        Signal.getTimeValue(self, nameOfValue, content)
        return self

    def getSignalDF(self, *columns):
        """Method returns a pandas DataFrame based on internal time and value properties"""
        return pd.DataFrame(self.Value, self.Time, columns=[col for col in columns])

    def read(self, signal=None, **kwargs):
        """Method returns a pandas DataFrame containing a signal with an index formatted to ns"""
        self.parseSignal()
        return self.getSignalDF(self.SignalName)

    def getTime(self):
        """Method returns the Time property"""
        return self.Time

    def getValue(self):
        """Method returns Value property"""
        return self.Value

    def getNamesOfValue(self, content):
        """Method returns a list of value names contained in content"""

        size = len(content['namesAndValues'])
        nameOfValue = ["" for x in range(0, size)]
        nameOfValue[:] = [content['namesAndValues'][n]['name'] for n in range(0, size)]

        return nameOfValue

    def getUnit(self, content):
        """ Method saves Parameters from PM Database into Class """

        try:  # there is not always a unit
            return content['valueDescriptor']['attributes'][self.SignalName]['units']['value']
        except:
            return 'N/A'

    def getTimeValue(self, nameOfValue, content):
        """ Method sets time (in ns) and value from the content received with a PM signal query  """

        self.EventTime = content['rawDataEntry']['pmEventStamp']
        for s in range(0, len(nameOfValue)):
            if self.SignalName in nameOfValue[s]:  # search for SignalName
                self.Value = content['namesAndValues'][s]['value']
            elif "START_TIME_NSEC" in nameOfValue[s]:  # search for StartTime
                self.StartTime = content['namesAndValues'][s]['value']
            elif "INTERVAL_NSEC" in nameOfValue[s]:  # search for Inteval
                self.IntervTime = content['namesAndValues'][s]['value']
            elif "AQNTIME" or "TIMESTAMP" in nameOfValue[s]:  # search for Inteval
                self.Time = content['namesAndValues'][s]['value']
            else:
                raise ValueError('Keyword ' + nameOfValue[s] + ' not supported for this type of query.')

        if self.Time == {}:  # Time empty, has to be generated from startTime and intervTime
            self.Time = [t for t in
                         range(self.StartTime, self.StartTime + self.IntervTime * len(self.Value), self.IntervTime)]