import pandas as pd
from datetime import datetime
import pytimber
from utilities.signal.df_signal import DFSignal
from utilities.signal.dataFrameIndexConverter import DataFrameIndexConverter

class Signal(DFSignal):

    def __init__(self, name={}, startTime=0, endTime=0, value=0, time=0, unit={}, ldb=None):
        super().__init__('CALS')
        self.Name = name
        self.StartTime = startTime
        self.EndTime = endTime
        self.Value = value
        self.Time = time
        self.Unit = unit
        self._ldb = ldb
        if self._ldb is None:
            self._ldb = pytimber.LoggingDB()

    @staticmethod
    def createSignalCALS(**kwargs):
        """Method validates kwargs and returns a Signal object.
        The method looks though kwargs for arguments to create a new Signal. If arguments are not provided, default
        arguments are used"""
        name = Signal.validateKeyInkwargs('name', str, list, **kwargs) or {}
        startTime = Signal.validateKeyInkwargs('startTime', str, float, datetime, **kwargs) or 0
        endTime = Signal.validateKeyInkwargs('endTime', str, float, datetime, **kwargs) or 0
        value = Signal.validateKeyInkwargs('value', list, **kwargs) or 0
        time = Signal.validateKeyInkwargs('time', list, **kwargs) or 0
        unit = Signal.validateKeyInkwargs('unit', str, **kwargs) or {}

        ldb_ = None
        if 'ldb' in kwargs:
            ldb_ = kwargs['ldb']

        if isinstance(name, str):
            name = [name]
        return Signal(name, startTime, endTime, value, time, unit, ldb_)

    def read(self, signal=None, **kwargs):
        """Method returns a pandas DataFrame containing all signals  with an index formatted to ns"""
        df = self.getSignalDF()

        if df is not None:
            DataFrameIndexConverter.convertIndexFromSecondsToNanoseconds(df)

        return df

    def getSignalDF(self):
        """Method returns a pandas DataFrame with signals"""
        signalDF = self.getSignal(self.Name[0])
        for s in range(1, len(self.Name)):
            newFrame = self.getSignal(self.Name[s])
            signalDF = pd.concat([signalDF, newFrame], axis=1)
        return signalDF

    def getSignal(self, name):
        """ Method returns a pandas DataFrame with one signal"""
        d = self._ldb.get(name, self.StartTime, self.EndTime)

        self.Time = d.get(name)[0]
        self.Value = d.get(name)[1]
        frame = pd.DataFrame(self.Value, self.Time, [name])
        return frame
