"""Sine wave example from: https://github.com/influxdata/influxdb-python/blob/master/examples/tutorial_sine_wave.py"""
from influxdb import InfluxDBClient
import math
import datetime
import time

client = InfluxDBClient(host='dbod-lhc-sm.cern.ch',
                        port=8081,
                        username='root',
                        password='root',
                        database='test',
                        ssl=True)


now = datetime.datetime.today()
points = []

for angle in range(0, 360):
    y = 10 + math.sin(math.radians(angle)) * 10

    point = {
        "measurement": 'foobar',
        "time": int(now.strftime('%S')) + angle,
        "fields": {
            "value": y
        }
    }
    points.append(point)

print("Writing points to database")
client.write_points(points)
print('done')

time.sleep(5)

query = 'SELECT * FROM foobar'
print("Querying data: " + query)
result = client.query(query)
print("Result: {0}".format(result))

