from abc import ABC, abstractclassmethod

class DFSignal(ABC):
    """ Base class for encapsulating the read and write methods of the Signal class"""

    def __init__(self, name=''):
        self.name = name

    def write(self, signal=None, **kwargs):
        """ Base implementation for write method. Defaults to telling the user, the api can't be written to."""

        print("Can not write to " + self.name)

    @staticmethod
    def validateKeyInkwargs(key, *keyTypes, **kwargs):
        result = None
        if key in kwargs:
            result = kwargs[key]
            matchFound = False
            for keyType in keyTypes:
                matchFound = matchFound or isinstance(result, keyType)
            if not matchFound:
                raise TypeError("You entered a {0} keyword, but the keyword needs to be a {1}.".format(key, keyTypes))

        return result