import unittest
from unittest import mock
import pytimber
import pandas as pd
from copy import deepcopy
from utilities.cals.signalCALS import Signal as SignalCALS
from utilities.signal.dataFrameIndexConverter import DataFrameIndexConverter


class TestSignalCALS(unittest.TestCase):

    def setUp(self):
        self.mockLoggingDB = mock.Mock()
        self.defaultCALSSignal = SignalCALS(ldb=self.mockLoggingDB)
        self.df = self.createTestDataFrame()
        self.calsDF = deepcopy(self.df)
        DataFrameIndexConverter.convertIndexFromNanosecondsToSeconds(self.calsDF)

    def tearDown(self):
        pass

    def createTestDataFrame(self):
        """Method creates a simple dataFrame"""
        data = [1.0, 1.2, 1.4]

        df = pd.DataFrame(data, [0, 1, 2], columns=['test'])
        return df


    def assertEqualSignals(self, firstCALSSignal, secondCALSSignal):
        """Method assert each property of firstCALSSignal against the the same property of secondCALSSignal"""
        self.assertEqual(firstCALSSignal.Name, secondCALSSignal.Name)
        self.assertEqual(firstCALSSignal.StartTime, secondCALSSignal.StartTime)
        self.assertEqual(firstCALSSignal.EndTime, secondCALSSignal.EndTime)
        self.assertEqual(firstCALSSignal.Value, secondCALSSignal.Value)
        self.assertEqual(firstCALSSignal.Time, secondCALSSignal.Time)
        self.assertEqual(firstCALSSignal.Unit, secondCALSSignal.Unit)


    def test_createCALSSignal_noParameters_defaultCALSBehaviorCreated(self):
        """Tests that the default Signal is created if no arguments are passed to the createSignalCALS method"""
        calsSignal = SignalCALS.createSignalCALS(ldb=self.mockLoggingDB)
        self.assertEqualSignals(calsSignal, self.defaultCALSSignal)

    def test_createCALSSignal_initParametersProvided_CorrectCALSBehaviorCreated(self):
        """Tests that the correct Signal is created if arguments are passed to the createSignalCALS method"""
        
        # assert:
        arbitraryCALSSignal = SignalCALS(["test"], 0.0, 1.0, ldb=self.mockLoggingDB) # common situation is name startTime and endTime

        # act:
        calsSignal = SignalCALS.createSignalCALS(name='test', startTime=0.0, endTime=1.0, ldb=self.mockLoggingDB)

        # assert:
        self.assertEqualSignals(calsSignal, arbitraryCALSSignal)

    def test_createCALSSignal_parameterIsInvalidType_TypeErrorIsThrown(self):
        """Tests when a keyword that is of an invalid type, a TypeError is thrown"""

        with self.assertRaises(TypeError):
            SignalCALS.createSignalCALS(name='test', startTime=0, endTime=1)


    def test_getSignal_returnsCorrectDataFrame(self):
        """Tests that getSignal returns correct pandas DataFrame"""
        
        # arrange:
        queryResult = {'test': [[0.0, 1e-09, 2e-09], [1.0, 1.2, 1.4]]}

        self.mockLoggingDB.get.return_value = queryResult

        uut = SignalCALS('test', 0.0, 3e-09, ldb=self.mockLoggingDB)

        # act
        result = uut.getSignal('test')

        # assert that pytimber LoggingDB's get method was called with the right arguments
        self.mockLoggingDB.get.assert_called_with('test', 0.0, 3e-09)

        # assert that a correctly formatted DataFrame is returned
        pd.testing.assert_frame_equal(result, self.calsDF)


    def test_getSignalDF_correctDataFrame(self):
        """Tests that getSignalDF returns correct pandas DataFrame consisting of more than one signal"""

        # arrange:createSignalCALS
        index = [0.0, 1e-09, 2e-09]
        values = [1.0, 1.2, 1.4]
        columns = ['signal1', 'signal2', 'signal3']

        self.mockLoggingDB.get.side_effect = [{columns[0]: [index, values]},
                                              {columns[1]: [index, values]},
                                              {columns[2]: [index, values]}]

        df = pd.DataFrame({columns[0]: values, columns[1]: values, columns[2]: values}, index)

        uut = SignalCALS(['signal1', 'signal2', 'signal3'], 0.0, 3e-09, ldb=self.mockLoggingDB)

        # act
        result = uut.getSignalDF()

        # assert that pytimber LoggingDB's get method was called with the right arguments
        self.mockLoggingDB.get.assert_any_call('signal1', 0.0, 3e-09)
        self.mockLoggingDB.get.assert_any_call('signal2', 0.0, 3e-09)
        self.mockLoggingDB.get.assert_any_call('signal3', 0.0, 3e-09)

        # assert that a correctly formatted DataFrame is returned
        pd.testing.assert_frame_equal(result, df)

    def test_read_multipleNamesProvided_returnsCorrectDataFrameWithIndexInNanoseconds(self):
        """Tests that read method correctly get a the correct DataFrame. That it then converts the index
        to nanoseconds, when the object is created with multiple signal names"""
        # arrange:
        index = [0.0, 1e-09, 2e-09]
        values = [1.0, 1.2, 1.4]
        columns = ['signal1', 'signal2', 'signal3']

        self.mockLoggingDB.get.side_effect = [{columns[0]: [index, values]},
                                              {columns[1]: [index, values]},
                                              {columns[2]: [index, values]}]

        df = pd.DataFrame({columns[0]: values, columns[1]: values, columns[2]: values}, index)
        DataFrameIndexConverter.convertIndexFromSecondsToNanoseconds(df)

        uut = SignalCALS(['signal1', 'signal2', 'signal3'], 0.0, 3e-09, ldb=self.mockLoggingDB)

        # act
        result = uut.read()

        # assert that pytimber LoggingDB's get method was called with the right arguments
        self.mockLoggingDB.get.assert_any_call('signal1', 0.0, 3e-09)
        self.mockLoggingDB.get.assert_any_call('signal2', 0.0, 3e-09)
        self.mockLoggingDB.get.assert_any_call('signal3', 0.0, 3e-09)

        # assert that a correctly formatted DataFrame is returned
        pd.testing.assert_frame_equal(result, df)

    def test_read_singleNameProvided_returnsCorrectDataFrameWithIndexInNanoseconds(self):
        """Tests that read method correctly get a the correct DataFrame. That it then converts the index
        to nanoseconds, when the object is created with a single signal name"""

        self.mockLoggingDB.get.return_value = {'test': [[0.0, 1e-09, 2e-09], [1.0, 1.2, 1.4]]}

        uut = SignalCALS(['test'], 0.0, 3e-09, ldb=self.mockLoggingDB)

        # act
        result = uut.read()

        # assert that pytimber LoggingDB's get method was called with the right arguments
        self.mockLoggingDB.get.assert_called_with('test', 0.0, 3e-09)

        # assert that a correctly formatted DataFrame is returned
        pd.testing.assert_frame_equal(result, self.df)
