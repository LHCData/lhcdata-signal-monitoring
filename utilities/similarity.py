import math

import numpy as np


# The Functions L1Distance, L2Distance, and LinfDistance expect two arrays of the same size.

def L1Distance(s1, s2):
	x = []
	for i in range(len(s1)):
		x.append(abs(s1[i] - s2[i]))
	return (float(sum(x)))


def L2Distance(s1, s2):
	x = []
	for i in range(len(s1)):
		x.append((s1[i] - s2[i]) ** 2)

	return (float(sum(x)) ** 0.5)


def LinfDistance(s1, s2):
	x = []
	for i in range(len(s1)):
		x.append(abs(s1[i] - s2[i]))
	return (float(max(x)))


def DTWDistance(s1, s2):
	DTW = {}

	for i in range(len(s1)):
		DTW[(i, -1)] = float('inf')
	for i in range(len(s2)):
		DTW[(-1, i)] = float('inf')
	DTW[(-1, -1)] = 0

	for i in range(len(s1)):
		for j in range(len(s2)):
			dist = (s1[i] - s2[j]) ** 2
			DTW[(i, j)] = dist + min(DTW[(i - 1, j)], DTW[(i, j - 1)], DTW[(i - 1, j - 1)])

	return math.sqrt(DTW[len(s1) - 1, len(s2) - 1])


def CorrrelateDistance(s1, s2):
	return float(np.correlate(s1[:, 0], s2[:, 0]))
